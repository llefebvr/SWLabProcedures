// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2016, 2017, 2018, ESRF - The European Synchroton, Gael Le Bec, Christophe Penel, Loic Lefebvre
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.6.4
// 11/01/2018
//
#pragma rtGlobals=1		// Use modern global access method.

Menu "SWLab Measurement"
	"Abort", MultipoleAbort()
	"-----------------"
	SubMenu "Initialization"
		"Open", MultipoleOpen()
		"Close", MultipoleClose()
	End
	SubMenu "Measurement parameters"
		"Load", MultipoleLoadParam()
		"Save", MultipoleSaveParam()
	End
	"Metadata", MultipoleMetaData()
	"-----------------"
	SubMenu "Alignment"
		"4-poles", MultipoleQuadAlignment()
	End
	"Set Centre and Axis to Last Measured Values", MultipoleSetCentreAxis()	
	"Reset Centre and Axis", MultipoleResetCentreAxis()	
	"-----------------"
	SubMenu "Circular Measurement"
		"Circular Measurement", CI(1,1)
		SubMenu "Radius"
			"New Radius Wave", MultipoleNewRadius()
			"Edit", Edit root:Multipole:Radius
		End
	End
	SubMenu "Other Measurements"
		"Compensated Measurement", CCI()
		"Linear Measurement",LI()
	End
	"----------------------------------"	
	SubMenu "Tools"
		"Measurement in Progress?" , MultipoleIsMeasuring()
		"Get Last Measurement",MultipoleGetLastMeas()
		"Get Last Error", MultipoleLastErr()
		"Quiet Mode", MultiQuiet(1)
		"Background sampling", MultipoleInitBackgroundSampling(16, 600, 1, 0)
	End
end
//-----------------------------------------------------------------------------------
// MultipoleCheckOpen()
//-----------------------------------------------------------------------------------
// Function MultipoleCheckOpen()
// NVAR/Z MultipoleInit = root:Multipole:MultipoleInit
	// If ( !NVAR_Exists(MultipoleInit) )
		// Variable/G root:Multipole:MultipoleInit = 0
	// EndIf
	// if (MultipoleInit==1)
	// print "Multipole measurement already initialised"
	// else	
		// MultipoleOpen()
		// Variable/G root:Multipole:MultipoleInit = 1
	// endif
	// SetDataFolder root:Multipole
// End

//-----------------------------------------------------------------------------------
// MultipoleOpen()
//-----------------------------------------------------------------------------------
Function MultipoleOpen()
	
	Print "-----------------------------------------------------------------------------"
	Print "Welcome to Stretched Wire Lab (SWLab)"
	Print "Version 1.6.4 / 1.4 (Procedures / Extensions)"
	Print " "
	Print "Copyright ESRF 2015-2018"
	Print "SWLab is under the terms of the GNU General Public License"
	Print " "
	Print "Please check on ftp://ftp.esrf.fr/pub/InsertionDevices/SWLab/ for updates and manual"
	Print "-----------------------------------------------------------------------------"
	NVAR/Z MultipoleInit = root:Multipole:MultipoleInit
	If ( !NVAR_Exists(MultipoleInit) )
		Variable/G root:Multipole:MultipoleInit = 1
	EndIf
	Wave/Z Radius = root:Multipole:Radius
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z Current = root:Multipole:Current
	String company = ""
	String operator = ""
	String polename = ""
	String npole="4-poles"
	String defaultDir = "C:\\Users\\SWUSER\\Documents\\Measurements"
	
	If (WaveExists(Radius))
		Duplicate Radius RadiusTmp
	EndIf
	If (WaveExists(MeasStr))
		Duplicate MeasStr MeasStrTmp
	EndIf
	If (WaveExists(Current))
		Duplicate Current CurrentTmp
	EndIf
	// Open Devices 
	axis2dopen("XPS2d")
	keithopen("K2182")
	// Initialize Measurement
	MultiOpen("Multipole")
	// Initialize Waves	
	If (DataFolderExists("root:Multipole"))
		SetDataFolder root:Multipole
	EndIf
	If ( WaveExists(RadiusTmp) )
		duplicate/o RadiusTmp Radius
		killwaves/z RadiusTmp
	EndIf
	If(WaveExists(CurrentTmp))
		duplicate/o CurrentTmp Current
		killwaves/z CurrentTmp
	Else 
		make/o/n=1 Current
	EndIf
	If (WaveExists(MeasStrTmp))
		duplicate/o/t MeasStrTmp MeasStr
		killwaves/z MeasStrTmp
	Else
		Make/o/t/n=7 MeasStr
		Prompt company, "Company"
		Prompt operator, "Operator"
		Prompt npole,"N-poles"
		Prompt polename, "Multipole Serial Number"
		Prompt defaultDir, "Measurement Directory"
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 					No need for Tesla							LL 17/03/2017 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//		DoPrompt "Initialization", operator, company, npole, polename, defaultDir
//		If (V_Flag)
//			killwaves/z MeasStr
//			MultipoleClose()	
//			Return -1					
//		EndIf
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		MeasStr[0]=company
		MeasStr[1]=operator
		MeasStr[4]  = polename
		MeasStr[5] = npole
		MeasStr[6] = defaultDir
	EndIf
	Make/O/n=2 root:Multipole:MagCentre
	Make/O/n=2 root:Multipole:MagAxis
	// Edit root:Multipole:Radius, root:Multipole:MagCentre
	// Set the default directory
	NewPath/O/Q/Z Path0, defaultDir
	PathInfo/S Path0
End

//-----------------------------------------------------------------------------------
// MultipoleClose()
//-----------------------------------------------------------------------------------
Function MultipoleClose()
NVAR/Z MultipoleInit = root:Multipole:MultipoleInit
	Variable/G root:Multipole:MultipoleInit = 0
	// Close Devices
	NVAR/z xpsPath = root:xps2d:path
	Axis2dClose(xpsPath)
	// K2182
	NVAR/z KPath = root:K2182:path
	KeithClose(KPath)
	// Close
	MultiClose()
End

//--------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------
Function MultipoleMetaData()
	Wave/T MeasStr = root:Multipole:MeasStr
	If(!WaveExists(MeasStr))
		Print "Multipole measurement not initialized"
		Return -1
	EndIf

	String company = MeasStr[0]
	String operator = MeasStr[1]
	String serialNum = MeasStr[4]
	String nPole = MeasStr[5]
	String defaultDir = MeasStr[6]
	
	Prompt company, "Company"
	Prompt operator, "Operator"
	Prompt npole,"N-poles"
	Prompt serialNum, "Multipole Serial Number"
	//Prompt defaultDir, "Measurement Directory"
	DoPrompt "Magnet name", operator, company, npole, serialNum//, defaultDir
	If (V_Flag)
		Return -1							
	EndIf

	MeasStr[0]=company
	MeasStr[1]=operator
	MeasStr[4]  = serialNum
	MeasStr[5] = npole
	MeasStr[6] = defaultDir
	
	// Set the default directory
	NewPath/O/Q/Z Path0, defaultDir
	PathInfo/S Path0
	
End


//--------------------------------------------------------------------------------------------
// MultipoleSaveParam()
//--------------------------------------------------------------------------------------------
Function MultipoleSaveParam()
	
	String path = "C:\ConfigurationFolder\MeasParameters"
	String name = "Untitled"
//	Prompt name,"File name"
//	Prompt path, "Path name"
//	DoPrompt "Save parameters", name, path
//	If (V_Flag)
//		Return -1							
//	EndIf

	Wave/Z BenchCentre = root:Multipole:BenchCentre
	Wave/Z MagCentre = root:Multipole:MagCentre
	Wave/Z BenchAxis = root:Multipole:BenchAxis
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z Radius = root:Multipole:Radius
	Wave/Z MeasParameters = root:Multipole:MeasParameters
	Wave/Z CompMeasParameters = root:Multipole:CompMeasParameters
	Wave/Z AnalysisParameters = root:Multipole:AnalysisParameters
	Wave/Z CompAnalysisParameters = root:Multipole:CompAnalysisParameters	
	Wave/Z RandRadiusParam = root:Multipole:RandRadiusParam	
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z/T AnalysisParametersStr = root:Multipole:AnalysisParametersStr
	Wave/Z CenterMeasParameters=root:Multipole:CenterMeasParameters
	Wave/Z ExportParameters = root:Multipole:ExportParameters
	Wave/Z DQParameters = root:Multipole:DQParameters
	Wave/Z ToleranceParameters = root:Multipole:ToleranceParameters
	Wave/Z XZAxisDirections = root:Multipole:XZAxisDirections
	
	// Update centre & Axis in every parameters
	If(WaveExists(MagCentre)&&WaveExists(MagAxis))
		MeasParameters[0] = MagCentre[0]
		MeasParameters[1] = MagCentre[1]
		CompMeasParameters[0] = MagCentre[0]
		CompMeasParameters[1] = MagCentre[1]
		CenterMeasParameters[0] = MagCentre[0]
		CenterMeasParameters[1] = MagCentre[1]
		CenterMeasParameters[2] = MagAxis[0]
		CenterMeasParameters[3] = MagAxis[1]
		AnalysisParameters[0] = WaveMax(Radius)
		CompAnalysisParameters[0] = WaveMax(Radius)
	EndIf
	
	// Initialize waves if needed
	If(!WaveExists(BenchCentre))
		If(!WaveExists(MagCentre))
			Make/n=2 root:Multipole:MagCentre
			Wave/Z MagCentre = root:Multipole:MagCentre
			MagCentre = 0
		// Else save MagCentre...
		EndIf
	Else 
		Duplicate/o BenchCentre, MagCentre
	EndIf
	If(!WaveExists(BenchAxis))
		If(!WaveExists(MagAxis))
			Make/n=2 root:Multipole:MagAxis
			Wave/Z MagAxis = root:Multipole:MagAxis
			MagAxis = 0
		// Else save MagAxis...
		EndIf
	Else 
		Duplicate/o BenchAxis, MagAxis
	EndIf	
	If(!WaveExists(Radius))
		Make/n=1 root:Multipole:Radius
		Wave/Z Radius = root:Multipole:Radius
		Radius[0] = 10
	EndIf	
	If(!WaveExists(MeasParameters))
		Make/n=9 root:Multipole:MeasParameters
		Wave/Z MeasParameters = root:Multipole:MeasParameters
		MeasParameters[0] = 0
		MeasParameters[1] = 0
		MeasParameters[2] = 128
		MeasParameters[3] = 2
		MeasParameters[4] = 0.04
		MeasParameters[5] = 5
		MeasParameters[6] = 80
		MeasParameters[7] = 2
		MeasParameters[8] = 150
	EndIf
	If(!WaveExists(CompMeasParameters))
		Make/n=8 root:Multipole:CompMeasParameters
		Wave/Z CompMeasParameters = root:Multipole:CompMeasParameters
		CompMeasParameters[0] = 0
		CompMeasParameters[1] = 0
		CompMeasParameters[2] = 64
		CompMeasParameters[3] = 4
		CompMeasParameters[4] = 2
		CompMeasParameters[5] = 5
		CompMeasParameters[6] = 80
	EndIf
	If(!WaveExists(AnalysisParameters))
		Make/n=7 root:Multipole:AnalysisParameters
		Wave/Z AnalysisParameters = root:Multipole:AnalysisParameters
		AnalysisParameters[0] = 10
		AnalysisParameters[1] = 2
		AnalysisParameters[2] = 16
		AnalysisParameters[3] = 0
		AnalysisParameters[4] = 0
		AnalysisParameters[5] = 0
		AnalysisParameters[6] = 0
	EndIf
	If(!WaveExists(CompAnalysisParameters))
		Make/n=7 root:Multipole:CompAnalysisParameters
		Wave/Z CompAnalysisParameters = root:Multipole:CompAnalysisParameters
		CompAnalysisParameters[0] = 10
		CompAnalysisParameters[1] = 2
		CompAnalysisParameters[2] = 16
		CompAnalysisParameters[3] = 0
		CompAnalysisParameters[4] = 0
		CompAnalysisParameters[5] = 0
		CompAnalysisParameters[6] = 0
	EndIf	
	If(!WaveExists(RandRadiusParam))
		Make/n=5 root:Multipole:RandRadiusParam
		Wave/Z RandRadiusParam = root:Multipole:RandRadiusParam
		RandRadiusParam[0] = 10
		RandRadiusParam[1] = 9
		RandRadiusParam[2] = 5
		RandRadiusParam[3] = 1
		RandRadiusParam[4] = 8
	EndIf
	If(!WaveExists(MeasStr))
		Make/T/n=7 root:Multipole:MeasStr
		Wave/Z/T MeasStr = root:Multipole:MeasStr
		MeasStr[0]="Company"
		MeasStr[1]="Operator"
		MeasStr[4]  = "Serial Number"
		MeasStr[5] = "4-poles"
		MeasStr[6] ="C:\\Users\\SWUSER\\Documents\\Measurements"
	EndIf
	If(!WaveExists(AnalysisParametersStr))
		Make/T/n=5 root:Multipole:AnalysisParametersStr
		Wave/Z/T AnalysisParametersStr = root:Multipole:AnalysisParametersStr
		AnalysisParametersStr[0]="FieldIntegral"
		AnalysisParametersStr[1]="TrajAngle"
		AnalysisParametersStr[2]  ="XPosition"
		AnalysisParametersStr[3]  ="ZPosition"
		AnalysisParametersStr[4] = "C:\ConfigurationFolder\PitchW--TO BE MEASURED IF NEEDED.ibw"
	EndIf
	If(!WaveExists(CenterMeasParameters))
		Make/n=9 root:Multipole:CenterMeasParameters
		Wave/Z CenterMeasParameters = root:Multipole:CenterMeasParameters
		CenterMeasParameters[0]=0
		CenterMeasParameters[1]=0
		CenterMeasParameters[2]=0
		CenterMeasParameters[3]=0
		CenterMeasParameters[4]=10
		CenterMeasParameters[5]=11
		CenterMeasParameters[6]=4
		CenterMeasParameters[7]=3
		CenterMeasParameters[8]=2
	EndIf
	If(!WaveExists(ExportParameters))
		Make/n=7 root:Multipole:ExportParameters
		Wave/Z ExportParam = root:Multipole:ExportParameters
		ExportParameters[0] = 0 
		ExportParameters[1] = 0
		ExportParameters[2] = 1
		ExportParameters[3] = 0
		ExportParameters[4] = 7
		ExportParameters[5] = 1
		ExportParameters[6]  = 600
	EndIf
	// Save measurement parameters
	NewPath/c/o/q path1, "C:\ConfigurationFolder\MeasParameters"
	String wNames
	String wPath="root:Multipole:"
	wNames = wpath+"MagCentre;"
	wNames += wpath+"MagAxis;"
	wNames += wpath+"Radius;"
	wNames += wpath+"MeasParameters;"
	wNames += wpath+"CompMeasParameters;"
	wNames += wpath+"AnalysisParameters;"
	wNames += wpath+"CompAnalysisParameters;"
	wNames += wpath+"RandRadiusParam;"
	wNames += wpath+"MeasStr;"
	wNames += wpath+"AnalysisParametersStr;"
	wNames += wpath+"CenterMeasParameters;"
	wNames += wpath+"ExportParameters;"
	If (WaveExists(DQParameters))
		wNames += wpath+"DQParameters;"
	EndIf
	If(WaveExists(ToleranceParameters))
		wNames += wpath+"ToleranceParameters;"
	EndIf
	If(WaveExists(XZAxisDirections))
		wNames += wpath+"XZAxisDirections;"
	EndIf
	Save/O/W/B/T/I/P=path1 wNames as name
	KillPath/z path1
	PathInfo/S Path0
End


//-----------------------------------------------------------------------------------
//  MultipoleLoadParam()
//-----------------------------------------------------------------------------------
Function MultipoleLoadParam()
	
	If (!DataFolderExists("root:Multipole:"))
		NewDataFolder root:Multipole
	EndIf
	// Load the wave parameters
	NewDataFolder/O root:Multipole:Tmp
	SetDataFolder root:Multipole:Tmp
	String path = "C:\ConfigurationFolder\MeasParameters"
	String name = "Untitled"
	NewPath/C/O/Q path1, "C:\ConfigurationFolder\MeasParameters"
	LoadWave/T/W/O/Q/A/I/K=0/P=path1, name
	If (!V_Flag)
		KillDataFolder/z root:Multipole:Tmp
		Return -1							
	EndIf
	KillPath/z path1
	SetDataFolder root:Multipole
	Wave/Z AnalysisParametersTmp = root:Multipole:Tmp:AnalysisParameters
	Wave/Z/T AnalysisParametersStrTmp = root:Multipole:Tmp:AnalysisParametersStr
	Wave/Z CompAnalysisParametersTmp = root:Multipole:Tmp:CompAnalysisParameters
	Wave/Z CompMeasParametersTmp = root:Multipole:Tmp:CompMeasParameters
	Wave/Z MagAxisTmp = root:Multipole:Tmp:MagAxis
	Wave/Z MagCentreTmp = root:Multipole:Tmp:MagCentre
	Wave/Z MeasParametersTmp = root:Multipole:Tmp:MeasParameters
	Wave/Z RadiusTmp = root:Multipole:Tmp:Radius		
	Wave/Z RandRadiusParamTmp = root:Multipole:Tmp:RandRadiusParam
	Wave/Z/T MeasStrTmp = root:Multipole:Tmp:MeasStr
	Wave/Z CenterMeasParametersTmp=root:Multipole:Tmp:CenterMeasParameters
	Wave/Z ExportParametersTmp=root:Multipole:Tmp:ExportParameters
	Wave/Z DQParametersTmp=root:Multipole:Tmp:DQParameters
	Wave/Z ToleranceParametersTmp = root:Multipole:Tmp:ToleranceParameters
	Wave/Z XZAxisDirectionsTmp = root:Multipole:Tmp:XZAxisDirections
	// Determine the number of non zero points of the Radius wave
	Variable k, npts=numPnts(RadiusTmp), nRadii
	For(k=0; k<npts; k+=1) 
		If (!RadiusTmp[k])
			Break
		EndIf
	EndFor
	nRadii=k
	// Create Parameter Waves
	Make/o/n=7 AnalysisParameters
	Make/o/T/n=5 AnalysisParametersStr
	Make/o/n=7 CompAnalysisParameters
	Make/o/n=8 CompMeasParameters
	Make/o/n=2 MagAxis
	Make/o/n=2 MagCentre
	Make/o/n=9 MeasParameters
	Make/o/T/n=7 MeasStr
	Make/o/n=5 RandRadiusParam
	Make/o/n=(nRadii) Radius
	Make/o/n=9 CenterMeasParameters
	Make/o/n=7 ExportParameters
	If (WaveExists(DQParametersTmp))
		Make/o/n=6 DQParameters
	EndIf
	If (WaveExists(ToleranceParametersTmp))
		Variable nTol = numpnts(ToleranceParametersTmp)
		Make/o/n=(nTol) ToleranceParameters
	EndIf
	If (WaveExists(XZAxisDirectionsTmp))
		Make/o/n=2 XZAxisDirections
		For (k=0; k<2; k+=1)
			XZAxisDirections[k] = XZAxisDirectionsTmp[k]
		EndFor
	EndIf
	// Fill the parameter waves
	For (k=0; k<2; k+=1)
		MagAxis[k] = MagAxisTmp[k]
		MagCentre[k] = MagCentreTmp[k]
	EndFor
	For (k=0; k<5; k+=1)
		AnalysisParametersStr[k] = AnalysisParametersStrTmp[k]
		RandRadiusParam[k] =RandRadiusParamTmp[k]
	EndFor
	For (k=0; k<7; k+=1)
		MeasStr[k] = MeasStrTmp[k]
		CompAnalysisParameters[k] = CompAnalysisParametersTmp[k]
		AnalysisParameters[k] = AnalysisParametersTmp[k]
		ExportParameters[k] = ExportParametersTmp[k]
	EndFor
	MeasStr[2] = ""
	MeasStr[3] = ""
	For (k=0; k<8; k+=1)
		CompMeasParameters[k] = CompMeasParametersTmp[k]
	EndFor		
	For (k=0; k<9; k+=1)
		MeasParameters[k] = MeasParametersTmp[k]
		CenterMeasParameters[k] = CenterMeasParametersTmp[k]
	EndFor
	For (k=0; k<nRadii; k+=1)
		Radius[k] = RadiusTmp[k]
	EndFor	
	For (k=0; k<nRadii; k+=1)
		Radius[k] = RadiusTmp[k]
	EndFor	
	If(WaveExists(DQParametersTmp))
		For (k=0; k<6; k+=1)
			DQParameters[k] = DQParametersTmp[k]
		EndFor
	EndIf
	If(WaveExists(ToleranceParametersTmp))
		For (k=0; k<numpnts(ToleranceParametersTmp); k+=1)
			ToleranceParameters[k] = ToleranceParametersTmp[k]
		EndFor
	EndIf
	Duplicate/o root:Multipole:MagCentre root:Multipole:BenchCentre 
	Duplicate/o root:Multipole:MagAxis root:Multipole:BenchAxis
	// Clear 
	KillDataFolder/z root:Multipole:Tmp
	PathInfo/S Path0
End 


//-----------------------------------------------------------------------------------
// MultipoleReset()
// Reset Devices
//-----------------------------------------------------------------------------------
Function MultipoleReset()
	 MultipoleClose()
	 MultipoleOpen()
End

//-----------------------------------------------------------------------------------
// MultipoleIsMeasuring()
//-----------------------------------------------------------------------------------
function MultipoleIsMeasuring()
	if (MultiIsMeasuring())
		print "Measuring"
		return 1
	else
		print "Not Measuring"
		return 0
	endif
end

//-----------------------------------------------------------------------------------
// MultipoleAbort()
//-----------------------------------------------------------------------------------
function MultipoleAbort()
	CtrlNamedBackground Check, kill
	CtrlNamedBackground CenterAxis kill
	CtrlNamedBackground CheckAlign kill
	CtrlNamedBackground _all_ kill = 1
	MultiAbort()
end



//-----------------------------------------------------------------------------------
// MultipoleLastErr()
//-----------------------------------------------------------------------------------
function /S MultipoleLastErr()
string err_ret
	err_ret=MultiGetLastError()
	print err_ret;
	return err_ret;
end

//-----------------------------------------------------------------------------------
//  MultipoleGetLastMeas()
//-----------------------------------------------------------------------------------
function MultipoleGetLastMeas()
	MultiGetLastMeas()
end

//----------------------------------------------------------------------------------
// LI()
// Linear Integral Measurements
//----------------------------------------------------------------------------------
proc LI(XBegin,ZBegin,XEnd,Zend,NumberOfPoints,IntegrationTime,Acceleration,NumberOfAverages)
Variable XBegin,ZBegin,XEnd,Zend,NumberOfPoints,IntegrationTime,Acceleration,NumberOfAverages
	MultiIntegralScan( XBegin,ZBegin,XEnd,Zend,NumberOfPoints,IntegrationTime,Acceleration,NumberOfAverages)
end

//-------------------------------------------------------------------------------------------------
// MultipoleNewRadius()
//-------------------------------------------------------------------------------------------------
Function MultipoleNewRadius()
	// Initialize
	Wave/Z RandRadiusParam = root:Multipole:RandRadiusParam
	Variable k
	Variable MaxRadius = 10
	Variable LargeRadius =9
	Variable SmallRadius = 5
	Variable std = 1
	Variable nRadius = 8
	If (WaveExists(RandRadiusParam))
		MaxRadius = RandRadiusParam[0]
		LargeRadius = RandRadiusParam[1]
		SmallRadius = RandRadiusParam[2]
		std = RandRadiusParam[3]
		nRadius = RandRadiusParam[4]
	EndIf
	// Dialog
	Prompt MaxRadius, "Maximum Radius [mm]" 
	Prompt LargeRadius, "Large Radius [mm]"
	Prompt SmallRadius, "Small Radius [mm]"
	Prompt std, "Standard Deviation @ Large Radius [mm]"
	Prompt nRadius, "Number of Radii"
	DoPrompt "New Radius Wave", MaxRadius, LargeRadius, SmallRadius, std, nRadius
	If (V_Flag)
		Return -1							
	EndIf
	//
	// Make Radius Wave
	Make/o/n=(nRadius) root:Multipole:Radius
	Wave/Z Radius=root:Multipole:Radius
	// Fill Radius Wave
	Variable n = log(nRadius)/log(LargeRadius/SmallRadius)
	Radius[0] = MaxRadius
	For(k=1; k<nRadius; k+=1)
		Radius[k] = LargeRadius*(k+1)^(1/n) /(  (nRadius)^(1/n)  )
		Radius[k] += gnoise(std * nRadius * (k+1) ^ (1/n-1) / nRadius^(1/n))
	//	Radius[k] = LargeRadius * log( ( 10 - 10^(SmallRadius/LargeRadius) ) / ( nRadius - 1 )*k + 10^(SmallRadius/LargeRadius) )
	//	Radius[k] += gnoise( std * 10 / (    (10 - 10^(SmallRadius/LargeRadius) ) / ( nRadius - 1 )*k + 10^(SmallRadius/LargeRadius) )   )
		Radius[k] = abs(Radius[k])
		If(Radius[k]>MaxRadius)
			Radius[k] = MaxRadius
		EndIf
	EndFor
	Sort/R Radius, Radius
	Make/o/n=5 root:Multipole:RandRadiusParam	
	Wave/Z RandRadiusParam = root:Multipole:RandRadiusParam
	RandRadiusParam[0] = MaxRadius
	RandRadiusParam[1] = LargeRadius
	RandRadiusParam[2] = SmallRadius
	RandRadiusParam[3] = std
	RandRadiusParam[4] = nRadius

End


//-------------------------------------------------------------------------------------------------
// CI(dialog, backg)
// Multiple Radius Circular Integral 
//
// Open a dialog if dialog = 1
// Start a backgroud task for getting the data if backg = 1
//-------------------------------------------------------------------------------------------------
Function CI(dialog, backg)
variable dialog, backg
	// Initialize
	String Rw = "root:Multipole:Radius"
	Wave/Z param = root:Multipole:MeasParameters
	Wave/Z radius = root:Multipole:Radius
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z MagCentre = root:Multipole:MagCentre
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z Current = root:Multipole:Current
	Variable XC, ZC, NBPT, pTI, Vel, Acc, R, NBTURN, XA,ZA,curr, driftCorr, f
	
	String operator
	String company
	String polename
	String npoleStr
	String defDir
	
	If ( !WaveExists(param) )
		XC = 0
		ZC = 0
		NBPT = 128
		NBTURN = 2
		pTI = 0.02
		Vel = 10
		Acc = 80
		driftCorr=2
		f = 150
	//	Make/o/n=1 root:Multipole:Radius
	//	radius[0] = 10
	Else 
		XC = MagCentre[0]
		ZC = Magcentre[1]
		NBPT = param[2]
		NBTURN = param[3]
		pTI = param[4]
		Vel = param[5]
		Acc = param[6]
		driftCorr=param[7]
		f = param[8]
		R=WaveMax(Radius)
	EndIf
	If ( !WaveExists(Current) )
		curr = 0
	Else
		curr = Current[0]
	EndIf
	If ( !WaveExists(MeasStr))
		operator = ""
		company = "ESRF"
		polename = "Serial Number"
		nPoleStr = "4-poles"
		defDir = ""
	Else
		operator = MeasStr[1]
		company = MeasStr[0] 
		polename = MeasStr[4]
		nPoleStr = MeasStr[5]
		defDir = MeasStr[6]
	EndIf  
	If (WaveExists(MagCentre))
		XC=MagCentre[0]
		ZC=MagCentre[1]
		
	EndIf
	If(WaveExists(MagAxis))
		XA=MagAxis[0]
		ZA=MagAxis[1]
		
	EndIf
	//
	if (dialog)
		Prompt XC, "X Centre [mm]"
		Prompt ZC, "Z Centre [mm]"
		Prompt XA, "X Axis [mm]"
		Prompt ZA, "Z Axis [mm]"
		Prompt NBPT, "Points per turn"
		Prompt NBTURN, "Number of turns"
		Prompt pTI, "Integration time [s]"
		Prompt Vel, "Velocity [mm/s]"
		Prompt driftCorr, "Order of drift correction"
		Prompt curr, "Current [A]"
		DoPrompt "Multipole Measurement",  XC, ZC, XA, ZA, Vel, NBPT, NBTURN, pTI,driftCorr,curr
		If (V_Flag)
			Return -1							
		EndIf
	else
		param[0] = XC
		param[1] = ZC
		XC=param[0]
		ZC=param[1]
		NBPT=param[2]
		NBTURN=param[3]
		pTI=param[4]
		Vel=param[5]
		Acc=param[6]
		driftCorr=param[7]
	endif
	If (driftCorr>6)
		driftCorr=6
	EndIf
	// Set Axis
	NVAR/z path = root:xps2d:path
	Axis2dSetTaper(path, cmplx(XA, ZA))
	Do
		Sleep /T 2
	While ( Axis2dIsMoving(path) )
	// Launch Measurement
	Sort/R $Rw, $Rw
	MultiRepeatCircularScan( XC, ZC,$Rw,NBPT,NBTURN,pTI,Vel,Acc,driftCorr)
	// Save Parameters
	Make/o/n=9 root:Multipole:MeasParameters
	Wave/Z param = root:Multipole:MeasParameters
	Make/o/t/n=7 root:Multipole:MeasStr
	param[0]=XC
	param[1]=ZC
	magCentre[0] = XC
	magCentre[1] = ZC
	magAxis[0] = XA
	magAxis[1] = ZA
	param[2]=NBPT
	param[3]=NBTURN
	param[4]=pTI
	param[5]=Vel
	param[6]=Acc
	param[7]=driftCorr
	param[8] = f
	measStr[0] = company
	measStr[1] = operator
	MeasStr[4]  = polename 
	MeasStr[5] = nPoleStr
	MeasStr[6] = defDir
	Current[0] = curr
	// Launch background task for getting data at the end of the measurment
	If(backg)
		StartMutiCheckGetMeasBck()
	EndIf
end

//--------------------------------------------------------------------------------------------------------------
// P_CCI()
// Compensated Measurements
//--------------------------------------------------------------------------------------------------------------
proc P_CCI(X0, Z0,Rw, LEN,NBPT, NBAvg, V,Acc,CompOrder, Phi0)
Variable X0, Z0, LEN,NBPT, NBAvg,V, Acc,CompOrder, Phi0
string Rw="root:Multipole:Radius"
	MultiCompIntegralScan(X0, Z0,$Rw, LEN,NBPT, NBAvg,V, Acc,CompOrder, Phi0)
end

//--------------------------------------------------------------------------------------------------------------
// CCI()
// Compensated Measurements
//--------------------------------------------------------------------------------------------------------------
Function CCI()
	// Initialize
	Variable nbAvg
	Variable Length
	String Rw = "root:Multipole:Radius"
	Wave/Z param = root:Multipole:CompMeasParameters
	Wave/Z radius = root:Multipole:Radius
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z MagCentre = root:Multipole:MagCentre
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z Current = root:Multipole:Current
	Variable XC, ZC, NBPT, Vel, Acc, R, XA,ZA,curr
	Variable MagType = 4
	String operator 
	String company
	String polename
	String npoleStr
	String defDir
	
	If ( !WaveExists(param) )
		XC = 0
		ZC = 0
		NBPT = 128
		NBAvg = 4
		Length = 2
		Vel = 10
		Acc = 80
	Else 
		XC = param[0]
		ZC = param[1]
		NBPT = param[2]
		NBAvg = param[3]
		Length = param[4]
		Vel = param[5]
		Acc = param[6]
		R=Radius[0]
	EndIf
	If ( !WaveExists(Current) )
		curr = 0
	Else
		curr = Current[0]
	EndIf
	If ( !WaveExists(MeasStr))
		operator = ""
		company = "ESRF"
		polename = "Serial Number"
		nPoleStr = "4-poles"
		defDir = ""
	Else
		operator = MeasStr[1]
		company = MeasStr[0] 
		polename = MeasStr[4]
		nPoleStr = MeasStr[5]
		defDir = MeasStr[6]
	EndIf  
	If (WaveExists(MagCentre))
		XC=MagCentre[0]
		ZC=MagCentre[1]
	EndIf
	If(WaveExists(MagAxis))
		XA=MagAxis[0]
		ZA=MagAxis[1]
		
	EndIf
	// Magnet type
	If ( StringMatch(nPoleStr, "2-poles")||(StringMatch(nPoleStr, "2-pole") ))
		MagType = 1
	ElseIf  ( StringMatch(nPoleStr, "4-poles")||(StringMatch(nPoleStr, "4-pole") ))
		MagType = 2
	ElseIf  ( StringMatch(nPoleStr, "6-poles")||(StringMatch(nPoleStr, "6-pole") ))
		MagType = 3
	ElseIf  ( StringMatch(nPoleStr, "8-poles")||(StringMatch(nPoleStr, "8-pole") ))
		MagType = 4
	Else 
		Print "Wrong number of poles. Please check  the MeasStr wave."
	EndIf
	// Dialog
	Prompt XC, "X Centre [mm]"
	Prompt ZC, "Z Centre [mm]"
	Prompt XA, "X Axis [mm]"
	Prompt ZA, "Z Axis [mm]"
	Prompt NBPT, "Points per turn"
	Prompt nbavg, "Averages"
	Prompt curr, "Current [A]"
	Prompt length, "Integration length [mm]"
	DoPrompt "Multipole Measurement",  XC, ZC, XA, ZA, NBPT, nbavg, length, curr
	If (V_Flag)
		Return -1							
	EndIf
	// Set Axis
	NVAR/z path = root:xps2d:path
	Axis2dSetTaper(path, cmplx(XA, ZA))
	Do
		Sleep /T 2
	While ( Axis2dIsMoving(path) )
	// Launch Measurement
	MultiCompIntegralScan(XC, ZC, Radius, length, NBPT, NBAvg,Vel, Acc,magType, 0)
	// Save Parameters
	Make/o/n=8 root:Multipole:CompMeasParameters
	Wave/Z param = root:Multipole:CompMeasParameters
	Make/o/t/n=8 root:Multipole:MeasStr
	param[0]=XC
	param[1]=ZC
	param[2]=NBPT
	param[3]=NBAvg
	param[4]=Length
	param[5]=Vel
	param[6]=Acc
	measStr[0] = company
	measStr[1] = operator
	MeasStr[4]  = polename 
	MeasStr[5] = nPoleStr
	MeasStr[6] = defDir
	
	// Launch background task for getting data at the end of the measurment
	StartMutiCheckGetCompMeasBck()
End

//--------------------------------------------------------------------------------------------------------------
// Background functions for getting data 
// Circular Measurements
//--------------------------------------------------------------------------------------------------------------
// MutiCheckGetMeasBck(s)	
Function MutiCheckGetMeasBck(s)		// This is the function that will be called periodically
	STRUCT WMBackgroundStruct &s
	
	If( !MultiIsMeasuring() )
		If (!cmpStr(MultiGetLastError(),"No Error\n"))
			MultiGetLastMeas()
			Wave/Z/T MeasStr=root:Multipole:MeasStr
			MeasStr[2] = Date()
			MeasStr[3] = Time()
			//Print  Date()+", "+Time()
			SetDataFolder root:Multipole
			// Convert field integral to [T mm]
			//Wave FieldIntegral
			//FieldIntegral /= 1000
			// Analysis
			MPMultipoleAnalysisDialog(0)
		Else
			MultipoleLastErr()
		EndIf	
		CtrlNamedBackground Check, Stop		
	EndIf
	return 0	// Continue background task
End

// StartMutiCheckGetMeasBck
Function StartMutiCheckGetMeasBck()
	Variable numTicks = 60		// Run every seconds (60 ticks)
	CtrlNamedBackground Check, period=numTicks, proc=MutiCheckGetMeasBck
	CtrlNamedBackground Check, start
	//Print "Measurement started"
	//Print date()+", "+time()
End

//--------------------------------------------------------------------------------------------------------------
// Background functions for getting data 
// Compensated Measurements
//--------------------------------------------------------------------------------------------------------------
// MutiCheckGetCompMeasBck(s)	
Function MutiCheckGetCompMeasBck(s)		// This is the function that will be called periodically
	STRUCT WMBackgroundStruct &s
	
	If( !MultiIsMeasuring() )
		If (!cmpStr(MultiGetLastError(),"No Error\n"))
			MultiGetLastMeas()
			Wave/Z/T MeasStr=root:Multipole:MeasStr
			MeasStr[2] = Date()
			MeasStr[3] = Time()
			//Print  Date()+", "+Time()
			//SetDataFolder root:Multipole
			// Change field integral units
			//Wave FieldIntegral
			//FieldIntegral /= 1000
			// Analysis
			MPMultipoleCompAnalysisDialog(0)
		Else
			MultipoleLastErr()
			Print "Data may be available, please check with MultipoleGetLastMeas()"
		EndIf	
		CtrlNamedBackground Check, Stop		
	EndIf
	return 0	// Continue background task
End

// StartMutiCheckGetCompMeasBck
Function StartMutiCheckGetCompMeasBck()
	Variable numTicks = 60		// Run every seconds (60 ticks)
	CtrlNamedBackground Check, period=numTicks, proc=MutiCheckGetCompMeasBck
	CtrlNamedBackground Check, start
	Print "Measurement started"
	Print date()+", "+time()
End

//--------------------------------------------------------------------------------------------------------------
// Background functions for getting Center data
//--------------------------------------------------------------------------------------------------------------
Function CenterCheckGetMeasBck(s)		// This is the function that will be called periodically
	STRUCT WMBackgroundStruct &s
	
	If( !MultiIsMeasuring() )
		If (!cmpStr(MultiGetLastError(),"No Error\n"))
			MultiGetLastMeas()
			Wave/Z/T MeasStr=root:Multipole:MeasStr
			MeasStr[2] = Date()
			MeasStr[3] = Time()
			Print  Date()+", "+Time()
			wave/Z QC=root:Multipole:MagCentreLastMeas
			wave/Z QA=root:Multipole:MagAxisLastMeas
			If (WaveExists(QC))
				print " Centre: X = " ,QC[0] , " Z= "  ,QC[1]
			EndIf
			If(WaveExists(QA))
				print " Axis: XA = " ,QA[0] , " ZA=" ,QA[1]
			EndIf
			print "-------------------------------" 
		EndIf	
		CtrlNamedBackground Check, Stop		
	EndIf
	return 0	// Continue background task
End
function StartBacKCenterCheck()
	Variable numTicks = 60		// Run every seconds (60 ticks)
	CtrlNamedBackground Check, period=numTicks, proc=CenterCheckGetMeasBck
	CtrlNamedBackground Check, start
	Print "Measurement started"
	Print date()+", "+time()
End

//----------------------------------------------------------------------------------------
// MultipoleFindCenter()
// Legacy function
//
// The new function MultipoleQuadAlignment() gives faster and better results
//----------------------------------------------------------------------------------------
Function MultipoleFindCenter()
	
	Print "WARNING: THIS FUNCTION IS OBSOLETE. PLEASE USE MultipoleQuadAlignment()."
	// Initialize
	//String Rw = "root:Multipole:Radius"
	Wave/Z param = root:Multipole:CenterMeasParameters
	//wave radius=$Rw
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z Current = root:Multipole:Current
	Variable XC, ZC,NBPT,NBAVG,radVar,XA,ZA,poletype,curr
	
	String operator
	String company
	String polename
	
	
	If ( !WaveExists(param) )
		XC = 0
		ZC = 0
		XA=0
		ZA=0
		NBPT = 11
		RadVar=10
		NBAVG=5
	Else 
		XC = param[0]
		ZC = param[1]
		XA=param[2]
		ZA=param[3]
		radvar=param[4]
		NBPT = param[5]
		NBAVG = param[6]
	EndIf
	
	print "-------------------------------" 
	wave/Z QCentre=root:Multipole:MagCentre	
	if (waveExists(QCentre))
		XC=QCentre[0]
		ZC=QCentre[1]
		print "Current  Center : X = " ,XC , " Z= "  ,ZC
	endif
	wave/Z QAxis=root:Multipole:MagAxis
	if (waveExists(QAxis))
		XA=QAxis[0]
		ZA=QAxis[1]
		print "Current  Taper : X = " ,XA , " Z= "  ,ZA
	endif
	
	If ( !WaveExists(Current) )
		curr = 0
	Else
		curr = Current[0]
	EndIf
	If ( !WaveExists(MeasStr))
		operator = ""
		company = "ESRF"
		polename = "Serial Number"
	Else
		operator = MeasStr[1]
		company = MeasStr[0] 
		polename = MeasStr[4]
		
	EndIf  
	//
	Prompt Xc, "X Centre [mm]"
	Prompt Zc, "Z Centre [mm]"
	prompt XA " X Axis [mm]" 
	prompt ZA " Z Axis [mm]" 	
	Prompt RadVar, "Range [mm]"
	Prompt curr, "Current [A]"
	Prompt NBPT, "Number of points"
	Prompt NBAVG, "Averages"
	DoPrompt "Quad Centre Parameters",  Xc, Zc,XA,ZA, RadVar,curr, NBPT, NBAVG
	If (V_Flag)
		Return -1							
	EndIf
	// Launch Measurement
	multiquadcenter(XC,ZC,XA,ZA,RadVar,NBPT,NBAVG)
	// Save Parameters
	Make/o/n=9 root:Multipole:CenterMeasParameters
	Wave/Z param = root:Multipole:CenterMeasParameters
	Make/o/t/n=7 root:Multipole:MeasStr
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	param[0]=XC
	param[1]=ZC
	param[2]=XA
	param[3]=XA
	param[4]=RadVar
	param[5]=NBPT
	param[6]=NBAVG
	measStr[0] = company
	measStr[1] = operator
	MeasStr[4]  = polename 
	// Launch background task for getting data at the end of the measurment
	StartBacKCenterCheck()
end

//------------------------------------------------------------------------------------------
// MultipoleFindAxis()
// Legacy function
//
// The new function MultipoleQuadAlignment() gives faster and better results
//------------------------------------------------------------------------------------------
Function MultipoleFindAxis()
	
	Print "WARNING: THIS FUNCTION IS OBSOLETE. PLEASE USE MultipoleQuadAlignment()."
	// Initialize
	//String Rw = "root:Multipole:Radius"
	Wave/Z param = root:Multipole:CenterMeasParameters
	//wave radius=$Rw
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z Current = root:Multipole:Current
	
	Variable XC, ZC,NBPT,NBAVG,radVar,XA,ZA,curr
	
	String operator
	String company
	String polename
	
	If ( !WaveExists(param) )
		XC = 0
		ZC = 0
		XA=0
		ZA=0
		NBPT = 11
		RadVar = 10
		NBAVG=5
	Else 
		XC = param[0]
		ZC = param[1]
		XA=param[2]
		ZA=param[3]
		radvar= param[4]
		NBPT = param[5]
		NBAVG = param[6]
	EndIf
	wave/Z QCentre=root:Multipole:MagCentre
	wave/Z QAxis=root:Multipole:MagAxis
	print "-------------------------------" 
	if (waveExists(QCentre))
		XC=QCentre[0]
		ZC=QCentre[1]
		print "Current  Centre: X = " , XC , " Z= "  , ZC
	endif
	if (waveExists(QAxis))
		XA=QAxis[0]
		ZA=QAxis[1]
		print "Current  Taper : X = " ,XA , " Z= "  ,ZA
	endif
	
	If ( !WaveExists(Current) )
		curr = 0
	Else
		curr = Current[0]
	EndIf
	If ( !WaveExists(MeasStr))
		operator = ""
		company = "ESRF"
		polename = "Serial Number"
	Else
		operator = MeasStr[1]
		company = MeasStr[0] 
		polename = MeasStr[4]
	Endif
		
	//
	Prompt Xc, "X Centre [mm]"
	Prompt Zc, "Z Centre [mm]"
	prompt XA " X Axis [mm]" 
	prompt ZA " Z Axis [mm]" 	
	Prompt RadVar, "Range [mm]"
	Prompt curr,"Current [A]"
	Prompt NBPT, "Number of points"
	Prompt NBAVG, "Averages"
	DoPrompt "Quad Axis Parameters",  Xc, Zc,XA,ZA, RadVar,curr, NBPT, NBAVG
	If (V_Flag)
		Return -1							
	EndIf
	// Launch Measurement
	multiquadaxis(XC,ZC,XA,ZA,RAdVar,NBPT,NBAVG)
	// Save Parameters
	Make/o/n=9 root:Multipole:CenterMeasParameters
	Wave/Z param = root:Multipole:CenterMeasParameters
	Make/o/t/n=7 root:Multipole:MeasStr
	Wave/Z/T MeasStr =  root:Multipole:MeasStr
	param[0]=XC
	param[1]=ZC
	param[2]=XA
	param[3]=ZA
	param[4]=RadVar
	param[5]=NBPT
	param[6]=NBAVG
	measStr[0] = company
	measStr[1] = operator
	MeasStr[4]  = polename 
	// Launch background task for getting data at the end of the measurment
	StartBacKCenterCheck()
end

//------------------------------------------------------------------------------------------
// MultipoleFindCentreAxis()
// Legacy function
//
// The new function MultipoleQuadAlignment() gives faster and better results
//------------------------------------------------------------------------------------------
Function MultipoleFindCentreAxis()
	
	Print "WARNING: THIS FUNCTION IS OBSOLETE. PLEASE USE MultipoleQuadAlignment()."
	// Initialize
	//String Rw = "root:Multipole:Radius"
	Wave/Z param = root:Multipole:CenterMeasParameters
	//wave radius=$Rw
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	wave/Z Npole = root:Multipole:Npole
	wave/Z Current = root:Multipole:Current
	
	Variable XC, ZC,NBPT,NBAVG,radVar,XA,ZA,NBITER,curr
	
	String operator
	String company
	String polename

	If ( !WaveExists(param) )
		Make/o/n=9 root:Multipole:CenterMeasParameters
		Wave/Z param = root:Multipole:CenterMeasParameters
		param[0]=0
		param[1]=0
		param[2]=0
		param[3]=0
		 param[4]=10
		 param[5]=10
		 param[6]=5
		 param[7]=5
		  param[0]=0
	Endif 
		XC = param[0]
		ZC = param[1]
		XA=param[2]
		ZA=param[3]
		radVar = param[4]
		NBPT = param[5]
		NBAVG = param[6]
		NBITER=param[7]
	
	print "-------------------------------" 
	
	if (!waveExists(root:Multipole:MagCentre))
		make /N=2 root:Multipole:MagCentre
	endif 
	wave/Z QCentre=root:Multipole:MagCentre
	XC=QCentre[0]
	ZC=QCentre[1]

	if (!waveExists(root:Multipole:MagAxis))
		make /N=2 root:Multipole:MagAxis
	endif
	wave/Z QAxis=root:Multipole:MagAxis
	XA=QAxis[0]
	ZA=QAxis[1]
	if(! DataFolderExists("root:Multipole:IterativeAlignment"))
		NewdataFolder root:Multipole:IterativeAlignment
	endif
	
	If ( !WaveExists(Current) )
		curr = 0
	Else
		curr = Current[0]
	EndIf
	If ( !WaveExists(MeasStr))
		operator = ""
		company = "ESRF"
		polename = "Serial Number"
	Else
		operator = MeasStr[1]
		company = MeasStr[0] 
		polename = MeasStr[4]
		
	EndIf 
	
	//
	Prompt Xc, "X Centre [mm]"
	Prompt Zc, "Z Centre [mm]"
	prompt XA " X Axis [mm]" 
	prompt ZA " Z Axis [mm]" 	
	Prompt RadVar, "Range [mm]"
	Prompt curr, "Current [A]"
	Prompt NBPT, "Number of points"
	Prompt NBAVG, "Averages"
	Prompt NBITER," Number of iterations"
	DoPrompt "Quad Centre and Axis Parameters",  Xc, Zc,XA,ZA, RadVar,curr, NBPT, NBAVG,NBITER
	If (V_Flag)
		Return -1							
	EndIf
		 param[0] =XC
		param[1]=ZC
		param[2]=XA
		param[3]=ZA
		param[4]=Radvar
		 param[5]=NBPT 
		 param[6]=NBAVG
		 param[7]=NBITER 
	
	if(! Exists("root:Multipole:StepTask"))
		variable /G root:Multipole:CurrentTask 
	endif
	NVAR StepTask=root:Multipole:CurrentTask 
	Steptask=0
	
	if(! Exists("root:Multipole:CurrentIter"))
		variable /G root:Multipole:CurrentIter 
	endif
	NVAR CurentIter=root:Multipole:CurrentIter
	CurentIter=0
	make /O /N=(NBITER) root:Multipole:IterativeAlignment:MagCentreX
	make /O /N=(NBITER) root:Multipole:IterativeAlignment:MagCentreZ
	make /O /N=(NBITER) root:Multipole:IterativeAlignment:MagAxisX
	make /O /N=(NBITER) root:Multipole:IterativeAlignment:MagAxisZ
	make /O /N=2 root:Multipole:MagCentreLastMeas
	make /O /N=2 root:Multipole:MagAxisLastMeas
	wave/Z TXC= root:Multipole:IterativeAlignment:MagCentreX
	wave/Z TZC= root:Multipole:IterativeAlignment:MagCentreZ
	wave/Z TXA= root:Multipole:IterativeAlignment:MagAxisX
	wave/Z TZA= root:Multipole:IterativeAlignment:MagAxisZ
	txc=nAN;tzc=nan;txa=nan;tza=nan
	edit TXC,TZC,TXA,TZA
	Variable numTicks = 2 * 60		// Run every two seconds (120 ticks)
	
	CtrlNamedBackground MultipoleFindCentreAxis, period=numTicks, proc=MultipoleCenterAxisTask
	CtrlNamedBackground MultipoleFindCentreAxis, start
		
End

//------------------------------------------------------------------------------------------
// Multipole CenterAxisTask(s)
// Legacy function
//
// The new function MultipoleQuadAlignment() gives faster and better results
//------------------------------------------------------------------------------------------
Function MultipoleCenterAxisTask(s)
STRUCT WMBackgroundStruct &s

NVAR StepTask=root:Multipole:CurrentTask
Wave/Z Param=root:Multipole:CenterMeasParameters
Wave/Z QC=root:Multipole:MagCentreLastMeas
Wave/Z QA=root:Multipole:MagAxisLastMeas
NVAR CurrentIter=root:Multipole:CurrentIter


Variable NBITER=param[7]

wave/Z TXC= root:Multipole:IterativeAlignment:MagCentreX
wave/Z TZC= root:Multipole:IterativeAlignment:MagCentreZ
wave/Z TXA= root:Multipole:IterativeAlignment:MagAxisX
wave/Z TZA= root:Multipole:IterativeAlignment:MagAxisZ


strswitch(num2str(steptask))			// numeric switch
		case "0":			//prepare Power Supply	
			//FCenter();
			//param[2]=QT[0]
			//param[3]=QT[1]
			CurrentIter=0
			TXC=NAn
			TZC=NAn
			TXA=NAn
			TZA=Nan
			multiquadcenter(param[0],param[1],param[2],param[3],param[4],param[5],param[6])			
			stepTask=1
			return 0;		
			break 

		case "1" :
			If( !MultiIsMeasuring() )
				If (!cmpStr(MultiGetLastError(),"No Error\n"))
					MultiGetLastMeas()
					Wave/Z/T MeasStr=root:Multipole:MeasStr
					MeasStr[2] = Date()
					MeasStr[3] = Time()
					Print  Date()+", "+Time()
					StepTask=2
				else
					print " Error on first MultiQuadCenter() " ,param[0],param[1],param[2],param[3],param[4],param[5],param[6],")"
					MultipoleLastErr()
					killvariables CurrentIter,stepTask 
					Return(2)
				endif
				
			endif
			return 0
		break
		case "2" :
			param[0]=QC[0]
			param[1]=QC[1]
			multiquadAxis(param[0],param[1],param[2],param[3],param[4],param[5],param[6])
			StepTask=3
			return 0;
		Break
		case "3":			
			If( !MultiIsMeasuring() )
				If (!cmpStr(MultiGetLastError(),"No Error\n"))
					MultiGetLastMeas()
					Wave/Z/T MeasStr=root:Multipole:MeasStr
					MeasStr[2] = Date()
					MeasStr[3] = Time()
					
					TXC[CurrentIter]=QC[0]
					TZC[CurrentIter]=QC[1]
					TXA[CurrentIter]=QA[0]
					TZA[CurrentIter]=QA[1]
					StepTask=4
					return 0
				else
					print " error on first multiquadAxis( " ,param[0],param[1],param[2],param[3],param[4],param[5],param[6],")"
					MultipoleLastErr()
					killvariables CurrentIter,stepTask 
					Return(-1)
				endif
			endif
			return 0
		break
		 case  "4":
		 		//param[4]=param[4]/2
		 		//param[5]=ceil(param[5]/2)
		 		CurrentIter=1
		 		StepTask=5
		 		return 0
		case "5":
		 			if ( CurrentIter>=NBIter)
						StepTask=10
					else
						StepTask=6
					endif
		 	return 0
		 break 
		case "6":		// execute if case matches expression
			//FCenter();
			param[2]=QA[0]
			param[3]=QA[1]
			multiquadcenter(param[0],param[1],param[2],param[3],param[4]/2,ceil(param[5]/2),param[6])			
			StepTask=7
			return 0;
		break					// exit from switch
		case "7":
			If( !MultiIsMeasuring() )
				If (!cmpStr(MultiGetLastError(),"No Error\n"))
					MultiGetLastMeas()
					Wave/Z/T MeasStr=root:Multipole:MeasStr
					MeasStr[2] = Date()
					MeasStr[3] = Time()
					Print  Date()+", "+Time()
					StepTask=8
				else
					print " error on Iteration Num  " ,CurrentIter,"  multiquadcenter " ,param[0],param[1],param[2],param[3],param[4]/2,ceil(param[5]/2),param[6],")"
					MultipoleLastErr()
					killvariables CurrentIter,stepTask 
					Return(2)
				endif
				
			endif
			return 0
		break
		case "8" :
			param[0]=QC[0]
			param[1]=QC[1]
			multiquadAxis(param[0],param[1],param[2],param[3],param[4]/2,ceil(param[5]/2),param[6])
			StepTask=9
			return 0;
		Break
		case "9":			
			If( !MultiIsMeasuring() )
				If (!cmpStr(MultiGetLastError(),"No Error\n"))
					MultiGetLastMeas()
					Wave/Z/T MeasStr=root:Multipole:MeasStr
					MeasStr[2] = Date()
					MeasStr[3] = Time()
					TXC[CurrentIter]=QC[0]
					TZC[CurrentIter]=QC[1]
					TXA[CurrentIter]=QA[0]
					TZA[CurrentIter]=QA[1]
					CurrentIter=CurrentIter+1
					stepTask=5
					return 0
				else
					print " error on Iteration Num  " ,CurrentIter,"  multiquadAxis " ,param[0],param[1],param[2],param[3],param[4]/2,ceil(param[5]/2),param[6],")"
					MultipoleLastErr()
					killvariables CurrentIter,stepTask 
					return 2
					
				endif
			endif
			return 0
		break
		case "10": 
				Print  Date()+", "+Time()
				Wave/Z/T MeasStr=root:Multipole:MeasStr
				MeasStr[2] = Date()
				MeasStr[3] = Time()
				print " Center : X = " ,QC[0] , " Z= "  ,QC[1]
				print " Taper  :TX = " ,QA[0] , " TZ=" ,QA[1]
				print "-------------------------------" 
				killvariables CurrentIter,stepTask 

			
			return 1
		break
					// when no case matches
	endswitch

return 0

end


//--------------------------------------------------------------------------------------------------------------
// MultipoleSetCentreAxis()
//--------------------------------------------------------------------------------------------------------------
Function MultipoleSetCentreAxis()

// Set the Multipole Data Folder
SetDataFolder root:Multipole
// Set Centre and Axis
if(!DataFolderExists("MultipoleData" ))
	NewDataFolder   MultipoleData
endif
Wave/Z MagCentreLastMeas
Wave/Z MagAxisLastMeas
If(WaveExists(MagCentreLastMeas))
	Duplicate/o MagCentreLastMeas MagCentre
	Duplicate/o MagCentreLastMeas root:Multipole:MultipoleData:MagCentre
EndIf
If(WaveExists(MagAxisLastMeas))
	Duplicate/o MagAxisLastMeas MagAxis
	Duplicate/o MagAxisLastMeas root:Multipole:MultipoleData:MagAxis
EndIf

End

//--------------------------------------------------------------------------------------------------------------
// MultipoleResetCentreAxis()
//--------------------------------------------------------------------------------------------------------------
Function MultipoleResetCentreAxis()

// Set the Multipole Data Folder
SetDataFolder root:Multipole
// Reset Centre and Axis
Make/o/n=2 BenchCentre, BenchAxis
Duplicate/o MagCentre BenchCentre
Duplicate/o MagAxisLastMeas BenchAxis

End

//----------------------------------------------------------------------------------
// Misc. Functions & Procedures
//----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
//  Proc PIM()
//-----------------------------------------------------------------------------------
Proc PIM(X,Z,Len,Angle,V,Acc,nb)
variable X,Z,Len,Angle,V,Acc,nb
	MultiPonctualIntegralMeas(X,Z,Len,Angle,V,Acc,nb)
end

//-----------------------------------------------------------------------------------
// P2IM(X,Z,Len,Angle,V,Acc,nb)
//-----------------------------------------------------------------------------------
Proc P2IM(X,Z,Len,Angle,V,Acc,nb)
variable X,Z,Len,Angle,V,Acc,nb
	MultiPonctualDoubleIntegralMeas(X,Z,Len,Angle,V,Acc,nb)
end

//----------------------------------------------------------------------------------
// LIS_A()
//----------------------------------------------------------------------------------
proc LIS_A(XC,ZC,Len,Angle,NBPT,TINT,Acc,NBSCAN)
variable XC,ZC,Len,Angle,NBPT,TINT,Acc,NBSCAN
variable XB,ZB,XE,ZE
XB=XC-LEN*cos(Angle-pi/2)/2
ZB=ZC-LEN*SIN(Angle-pi/2)/2
XE=XC+LEN*cos(Angle-pi/2)/2
ZE=ZC+LEN*SIN(Angle-pi/2)/2
//printf "XB=%d  ZB=%d  XE=%d   ZE = %d \015 ", XB,ZB,XE,ZE
MultiIntegralScan( XB,ZB,XE,ZE,NBPT,TINT,Acc,NBScan)
end

//----------------------------------------------------------------------------------
// MultipoleQuadAlignment()
// GLB - 13/03/2017
//----------------------------------------------------------------------------------
Function MultipoleQuadAlignment()

	// Initialize ------------------------
	Wave/Z param = root:Multipole:CenterMeasParameters
	Wave/Z mParam = root:Multipole:MeasParameters
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	wave/Z Current = root:Multipole:Current
	String operator
	String company
	String polename
	String nPoleStr
	String defDir
	If ( !WaveExists(param) )
		Make/o/n=9 root:Multipole:CenterMeasParameters
		Wave/Z param = root:Multipole:CenterMeasParameters
		param[0]=0
		param[1]=0
		param[2]=0
		param[3]=0
		param[4]=10
		param[5]=1 // Legacy parameter; not used here 
		param[6]=4
		param[7]=1 // Legacy parameter; not used here
		param[8]=2
	Endif 
	Variable DR = param[4]
	Variable NBAVG = param[6]
	Variable LEN=param[8]
	If (WaveExists(mParam))
		Variable VEL = mParam[5]
		Variable ACC = mParam[6]
	EndIf
	If (!waveExists(root:Multipole:MagCentre))
		make /N=2 root:Multipole:MagCentre
	EndIf 
	Wave/Z QCentre=root:Multipole:MagCentre
	Variable XC=QCentre[0]
	Variable ZC=QCentre[1]

	If (!waveExists(root:Multipole:MagAxis))
		make /N=2 root:Multipole:MagAxis
	EndIf
	Wave/Z QAxis=root:Multipole:MagAxis
	Variable XA=QAxis[0]
	Variable ZA=QAxis[1]
	Variable curr
	
	If ( !WaveExists(Current) )
		make/o/n=1 root:Multipole:Current
		curr = 0
		wave Current= root:Multipole:Current
	Else
		curr = Current[0]
	EndIf
	If ( !WaveExists(MeasStr))
		operator = ""
		company = "ESRF"
		polename = "Serial Number"
		nPoleStr = ""
		defDir = ""
	Else
		operator = MeasStr[1]
		company = MeasStr[0] 
		polename = MeasStr[4]
		nPoleStr = MeasStr[5]
		defDir = MeasStr[6]
	EndIf 
	// Prompt measurement parameters
	Prompt XC, "X Centre [mm]"
	Prompt ZC, "Z Centre [mm]"
	prompt XA " X Axis [mm]" 
	prompt ZA " Z Axis [mm]" 	
	Prompt DR, "Radius range [mm]"
	Prompt LEN, "Integration length [mm]"
	Prompt VEL, "Velocity [mm/s]"
	Prompt ACC, "Acceleration [mm/s2]"
	Prompt NBAVG, "Averages"
	Prompt curr, "Current [A]"
	DoPrompt "Quadrupole Alignment Parameters",  XC, ZC, XA, ZA, DR, LEN, VEL, ACC, NBAVG, curr
	If (V_Flag)
		Return -1							
	EndIf
	Current[0] = CURR
	mParam[5] = VEL
	mParam[6] = ACC
	// Launch Measurement
	MultiQuadAlignment( XC, ZC, XA, ZA, DR, DR, LEN, VEL, ACC, NBAVG)
	// Save Parameters
	Make/o/n=9 root:Multipole:MeasParameters
	Wave/Z param = root:Multipole:MeasParameters
	Make/o/t/n=7 root:Multipole:MeasStr
	Make/o/n=9 root:Multipole:CenterMeasParameters
	Wave/Z param = root:Multipole:CenterMeasParameters
	param[0] = XC
	param[1] = ZC
	param[2] = XA
	param[3] = ZA
	param[4] = DR
	param[6] = NBAVG
	param[8] = LEN
	mParam[5] = VEL
	mParam[6] = ACC
	measStr[0] = company
	measStr[1] = operator
	MeasStr[4]  = polename 
	MeasStr[5] = nPoleStr
	MeasStr[6] = defDir
	Current[0] = curr
	// Launch background task for getting data at the end of the measurment
	Variable numTicks = 60		// Run every seconds (60 ticks)
	CtrlNamedBackground CheckAlign, period=numTicks, proc=MutiCheckQuadAlignBck
	CtrlNamedBackground CheckAlign, start
	Print "-------------------------------" 
	Print "Quadrupole alignment started"
	Print date()+", "+time()
	
End
//----------------------------------------------------------------------------------
// MutiCheckQuadAlignBck(s)	
//----------------------------------------------------------------------------------
Function MutiCheckQuadAlignBck(s)		
	STRUCT WMBackgroundStruct &s
	
	If( !MultiIsMeasuring() )
		If (!cmpStr(MultiGetLastError(),"No Error\n"))
			MultiGetLastMeas()
			Wave/Z/T MeasStr=root:Multipole:MeasStr
			Wave/Z QuadAlignResults = root:Multipole:QuadAlignResults
			// Store results
			MeasStr[2] = Date()
			MeasStr[3] = Time()
			Make/O/N=2 root:Multipole:MagCentreLastMeas
			Make/O/N=2 root:Multipole:MagAxisLastMeas			
			Wave/Z MagCentreLastMeas=root:Multipole:MagCentreLastMeas
			Wave/Z MagAxisLastMeas=root:Multipole:MagAxisLastMeas
			MagCentreLastMeas[0] = QuadAlignResults[0]
			MagCentreLastMeas[1] = QuadAlignResults[1]
			MagAxisLastMeas[0] = QuadAlignResults[2]			
			MagAxisLastMeas[1] = QuadAlignResults[3]			
			//Print results
			Print  Date()+", "+Time()
			Print "-------------------------------" 
			Print "MAGNETIC CENTRE"
			Print "X0: "+num2str(QuadAlignResults[0])+" mm"
			Print "Z0: "+num2str(QuadAlignResults[1])+" mm"
			//Print "S0: "+num2str(QuadAlignResults[5])+" mm"
			Print "S0: "+num2str(QuadAlignResults[4])+" mm"
			Print "-------------------------------" 
			Print "MAGNETIC AXIS"
			Print "DX: "+num2str(QuadAlignResults[2])+" mm"
			Print "DZ: "+num2str(QuadAlignResults[3])+" mm"
			Print "-------------------------------" 
			Print "MAGNETIC LENGTH"
			Print "Lm: "+num2str(QuadAlignResults[5])+" mm"
			Print "-------------------------------" 
			Print "WIRE LENGTH: "+num2str(QuadAlignResults[7])+" mm"
		Else
			MultipoleLastErr()
		EndIf	
		CtrlNamedBackground CheckAlign, Stop		
	EndIf
	return 0	// Continue background task
End




//----------------------------------------------------------------------------------
// The two functions below can be use to launch a circular
// measurement CI() at regular intervals.
// This must be done using background functions, as described below, 
// because CI() is an asynchronous command.
// Only the dipole component is sampled in the procedure below. 
// It should be considered as a template for more complex acquisitions.
//----------------------------------------------------------------------------------
// --------------------------
// MultipoleInitBackgroundSampling(n_meas0, time_step0, pooling_time0)
//
// Initialize all variables for the state machine and the measurement
// Start the measurement state machine
//
// n_meas0: number of measurement points
// time_step0: time interval between two measurement points [s]
// pooling_time: duration between two calls of the state machine [s]
// quiet: if 1, does not print the measurement results in the history
// --------------------------
Function MultipoleInitBackgroundSampling(n_meas0, time_step0, pooling_time0, quiet)
	Variable n_meas0, time_step0, pooling_time0, quiet
	
	//--- Initialize 
	NewDataFolder/O/S root:Multipole:Sampling // New folder
	
	//--- Prompt values
	Variable pooling_time = pooling_time0
	Variable sampling_n0 = n_meas0 
	Variable sampling_dt0 = time_step0
	Variable sampling_quiet0 = quiet
	Prompt sampling_n0, "Number of points" 
	Prompt sampling_dt0, "Measurement time step [s]"
	Prompt pooling_time, "Pooling time step [s]"
	Prompt sampling_quiet0, "Quiet (0: No, 1: Yes)"
	DoPrompt "Acquisition parameters", sampling_n0, sampling_dt0, pooling_time, sampling_quiet0
	If (V_Flag)
		Return -1							
	EndIf
	
	// State machine variables
	String/G sampling_state = "0"
	Variable/G sampling_i = 0
	Variable/G sampling_n = sampling_n0
	Variable/G sampling_dt = sampling_dt0
	Variable/G sampling_quiet = sampling_quiet0
	
	// Data
	Make/O/N=(sampling_n) MeasTime = NaN
	Make/O/N=(sampling_n) DipoleField = NaN
	
	//--- Start the measurement
	Variable numTicks = 60 * pooling_time0  // 60 ticks = 1 s
	CtrlNamedBackground Check, period=numTicks, proc=MultipoleBackgroundSampling
	CtrlNamedBackground Check, start
	
End
// --------------------------
// MultipoleBackgroundSampling(s)
// This function will be called periodically
// The following state machine is implemented:
//	STATE 0: start the measurement (asynchronous call of CI())
// 	STATE 1: wait for the end of the measurement
// 	STATE 2: stop the machine if the specified number of measurement is reached
//	STATE 3 wait for the next measurement
// --------------------------
Function MultipoleBackgroundSampling(s)		// This is the function that will be called periodically
	STRUCT WMBackgroundStruct &s
	
	SetDataFolder  root:Multipole:Sampling
	//--- Initialize and get waves and global variables
	SVAR sampling_state = root:Multipole:Sampling:sampling_state
	NVAR sampling_i = root:Multipole:Sampling:sampling_i
	NVAR sampling_n = root:Multipole:Sampling:sampling_n
	NVAR sampling_dt = root:Multipole:Sampling:sampling_dt
	NVAR sampling_quiet = root:Multipole:Sampling:sampling_quiet
	Wave MeasTime = root:Multipole:Sampling:MeasTime
	Wave DipoleField = root:Multipole:Sampling:DipoleField
	Variable MeasTimeVar
	
	//--- State machine
	StrSwitch(sampling_state)
		// STATE 0
		// Start the measurement
		Case "0":
			// Get time
			MeasTimeVar = DateTime	
			MeasTime[sampling_i] = MeasTimeVar 
			If (!sampling_quiet)
				print "---"
				print "Measurement "+num2str(sampling_i)+" started..." 
				Print Date()+", "+Time()
			EndIf
			// Start the measurement
			CI(0, 0)
			// Go to state 1
			sampling_state = "1"
			Break
		// STATE 1
		// Wait for the end of the measurement
		Case "1": 
			If( !MultiIsMeasuring() )
				// Get last measurement
				MultiGetLastMeas()
				SetDataFolder  root:Multipole
				MPMultipoleAnalysisDialog(0)
				SetDataFolder  root:Multipole:Sampling
				// Read the dipole
				Wave bn = root:Multipole:MultipoleData:bn
				DipoleField[sampling_i] = bn[0]
				// Print the result
				If (!sampling_quiet)
					print "Dipole: "+num2str(DipoleField[sampling_i])+" Tmm"
				EndIf
				// Uncomment the line below to save the measurement after each measurement
				//SaveExperiment
				// Go to state 2
				sampling_state = "2"
			EndIf
			Break
		// STATE 2
		// New measurement?
		Case "2":
		sampling_i += 1
			If (sampling_i < sampling_n )  // Go to state 3 if number of measurements not reached
				sampling_state = "3"
			Else // Else stop the backgroud sampling function
				print "---"
				print "MEASUREMENT DONE"
				CtrlNamedBackground Check, Stop	
			EndIf
			Break
		// STATE 3 
		// Wait for the next measurement
		Case "3":
			MeasTimeVar = DateTime	
			If (MeasTimeVar >= MeasTime[sampling_i - 1] + sampling_dt)
				sampling_state = "0"
			EndIf
		Break
 	EndSwitch
 	
	Return 0
End
