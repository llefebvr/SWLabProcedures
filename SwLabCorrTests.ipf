#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma version = 1.0
#pragma IgorVersion = 6.0
#include "tango"

Menu "SWLab Correctors Test"
	"On",EBSOn()
	"Off",EBSOff()
	"Reset",EBSReset()
End

//==============================================================================
// Reset
//==============================================================================
//	Function.......executes the <Reset> command
//	Dev.class......StaticBilt
//	Cmd.name.......Reset
//	Arg-in type....None
//	Arg-in desc....uninitialised
//	Arg-out type...None
//	Arg-out desc...uninitialised
//	Example........the following code shows how to use this function
//------------------------------------------------------------------------------
//	function myFunction ()
//		//- execute the command and check error
//		if (Reset() == kERROR)
//			//- an error occurred
//			return kERROR
//		endif
//		//- the command was successfully executed
//		//...
//		return kNO_ERROR
//	end
//==============================================================================
function EBSReset ()
	if (tango_cmd_inout("et/sta_bilt/01", "Reset") == kERROR)
		tango_print_error()
		return kERROR
	endif
	return kNO_ERROR
end

//==============================================================================
// On
//==============================================================================
//	Function.......executes the <On> command
//	Dev.class......StaticBilt
//	Cmd.name.......On
//	Arg-in type....None
//	Arg-in desc....uninitialised
//	Arg-out type...None
//	Arg-out desc...uninitialised
//	Example........the following code shows how to use this function
//------------------------------------------------------------------------------
//	function myFunction ()
//		//- execute the command and check error
//		if (On() == kERROR)
//			//- an error occurred
//			return kERROR
//		endif
//		//- the command was successfully executed
//		//...
//		return kNO_ERROR
//	end
//==============================================================================
function EBSOn ()
	if (tango_cmd_inout("et/sta_bilt/01", "On") == kERROR)
		tango_print_error()
		return kERROR
	endif
	return kNO_ERROR
end

//==============================================================================
// Off
//==============================================================================
//	Function.......executes the <Off> command
//	Dev.class......StaticBilt
//	Cmd.name.......Off
//	Arg-in type....None
//	Arg-in desc....uninitialised
//	Arg-out type...None
//	Arg-out desc...uninitialised
//	Example........the following code shows how to use this function
//------------------------------------------------------------------------------
//	function myFunction ()
//		//- execute the command and check error
//		if (Off() == kERROR)
//			//- an error occurred
//			return kERROR
//		endif
//		//- the command was successfully executed
//		//...
//		return kNO_ERROR
//	end
//==============================================================================
function EBSOff ()
	if (tango_cmd_inout("et/sta_bilt/01", "Off") == kERROR)
		tango_print_error()
		return kERROR
	endif
	return kNO_ERROR
end



//==============================================================================
// SetCurrents
//==============================================================================
//	Function......writes the specified value on the <Currents> attribute
//	Dev.class.....StaticBilt
//	Attr.name.....Currents
//	Attr.desc.....channels current
//	Attr.Access...READ-WRITE
//	Attr.Format...SPECTRUM
//	Attr.Type.....double spectrum (64-bits floats) [Igor 1D Wave/D]
//	Example.......the following code shows how to use this function
//------------------------------------------------------------------------------
//	function myFunction ()
//		//- make the source wave (must match the attribute data type)
//		Make/O/D/N=128 my_spectrum = enoise(1.0)
//		//- get full path to the source wave (i.e. wave location)
//		String src_wave_path = GetWavesDataFolder(my_spectrum, 2)
//		//- apply the value and check error
//		if (SetCurrents(src_wave_path) == kERROR)
//			//- an error occurred
//			return kERROR
//		endif
//		//- the value was successfully applied
//		return kNO_ERROR
//	end
//==============================================================================
function EBSSetCurrents (src_path)
	String src_path
	Struct AttributeValue av
	tango_init_attr_val(av, dev="et/sta_bilt/01", attr="Currents", path=src_path)
	if (tango_write_attr(av) == kERROR)
		tango_print_error()
		return kERROR
	endif
	return kNO_ERROR
end

//==============================================================================
// GetCurrents
//==============================================================================
//	Function......reads then returns the <Currents> attribute value
//	Features......may optionally returns both attribute timestamp and quality
//	Dev.class.....StaticBilt
//	Attr.name.....Currents
//	Attr.desc.....channels current
//	Attr.Access...READ-WRITE
//	Attr.Format...SPECTRUM
//	Attr.Type.....double spectrum (64-bits floats) [Igor 1D Wave/D]
//	Example.......the following code shows how to use this function
//------------------------------------------------------------------------------
//	function myFunction ()
//		//- tell the tango binding where to put the 1D 'destination' wave
//		//- here, we want the attr. value (i.e. the associated wave) to be
//		//- placed into <root:my_df:> and named <my_spectrum>
//		String dest_wave_path = "root:my_df:my_spectrum"
//		//- read the spectrum attribute and check error
//		if (GetCurrents(dest_wave_path) == kERROR)
//			//- an error occurred
//			return kERROR
//		endif
//		//- the spectrum attribute was successfully read 
//		WAVE currents_wave = $dest_wave_path
//		//...
//		return kNO_ERROR
//	end
//==============================================================================
function EBSGetCurrents (dest_path, [tms, qlt])
	String& dest_path
	Variable& tms
	Variable& qlt
	Struct AttributeValue av
	tango_init_attr_val(av, dev="et/sta_bilt/01", attr="Currents", path=dest_path)
	if (tango_read_attr(av) == kERROR)
		tango_print_error()
		dest_path = ""
		if (! ParamIsDefault(tms))
			tms = -1
		endif
		if (! ParamIsDefault(qlt))
			qlt = kAttrQualityUNKNOWN
		endif
		return kERROR
	endif
	dest_path = av.val_path
	if (! ParamIsDefault(tms))
		tms = av.ts
	endif
	if (! ParamIsDefault(qlt))
		qlt = av.quality
	endif
	return kNO_ERROR
end
