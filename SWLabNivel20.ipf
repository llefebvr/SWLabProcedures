#pragma rtGlobals=3		// Use modern global access method and strict wave access.


Menu "SWLab Nivel20"
	"Angle measurement", NivelMeasureAngles()
	"Angle values to set", NivelSetValue()
	"Angle values to set  -- manual", NivelSetValueMan()

End

// ------------------------------------------------------
// NivelReadX()
// ------------------------------------------------------
Function NivelReadX()

	VDTOpenPort2 COM2
	VDTTerminalPort2 COM2
	
	// Ask for X angle
	VDTWriteBinary2/O=1 22 
	VDTWriteBinary2/O=1 2
	VDTWrite2/O=1 "N0C1 G X"
	VDTWriteBinary2/O=1 3 
	VDTWriteBinary2/O=1 13
	VDTWriteBinary2/O=1 10
	
	Variable a
	String s

	// Read value
	VDTReadBinary2/O=1 a
	VDTReadBinary2/O=1 a
	VDTRead2/O=1/N=13 s
	VDTReadHex2/O=1 a
	VDTClosePort2 COM2

	// String processing
 	s = StringByKey("C1N0 X", s, ":", ",")
 	a = str2num(s)
 	
 	Return a
End


// ------------------------------------------------------
//  NivelReadY()
// ------------------------------------------------------
Function NivelReadY()

	VDTOpenPort2 COM2
	VDTTerminalPort2 COM2
	
	// Ask for X angle
	VDTWriteBinary2/O=1 22 
	VDTWriteBinary2/O=1 2
	VDTWrite2/O=1 "N0C1 G Y"
	VDTWriteBinary2/O=1 3 
	VDTWriteBinary2/O=1 13
	VDTWriteBinary2/O=1 10
	
	Variable a
	String s

	// Read value
	VDTReadBinary2/O=1 a
	VDTReadBinary2/O=1 a
	VDTRead2/O=1/N=13 s
	VDTReadHex2/O=1 a
	VDTClosePort2 COM2

	// String processing
 	s = StringByKey("C1N0 Y", s, ":", ",")
 	a = str2num(s)
 	
 	Return a
End


// ------------------------------------------------------
// NivelMeasureAngles()
// ------------------------------------------------------
Function/C NivelMeasureAngles()
	
	// Measurement one side
	Variable x, y, x1, x2, y1, y2
	DoAlert /T="NIVEL 20 INCLINOMETER" 0, "Install the NIVEL 20 on the - X side and click OK"

	x1=NivelReadX()
	y1=NivelReadY()
	
	
	// Measurement other side
	DoAlert /T="NIVEL 20 INCLINOMETER" 0, "Install the NIVEL 20 on the + X side and click OK"

	x2=NivelReadX()
	y2=NivelReadY()
	
	// Angles
	x = (x2-x1)/2
	y = (y2-y1)/2 
	Print "-------------------"
	Print "ANGLES"
	Print "Theta X: "+num2str(x)+" mrad"
	Print "Theta S: "+num2str(y)+" mrad"
	
	Return cmplx(x,y)
	
End


// ------------------------------------------------------
// NivelSetValue()
// ------------------------------------------------------
Function NivelSetValue()

	Wave roll = root:Multipole:MultipoleData:RollAngle
	Wave align = root:Multipole:quadAlignResults
	Wave/Z BenchAxis = root:Multipole:BenchAxis
	Wave/Z MagAxis = root:Multipole:MagAxis
	Variable dz = MagAxis[1]-BenchAxis[1]
	Variable pitch = dz / align[7] * 1000
	
	If( (!WaveExists(roll)) ||(!WaveExists(align)) )
		Return -1
	EndIf
	DoAlert /T="NIVEL 20 INCLINOMETER" 0, "Install the Inclinometer on the - X side and click OK"
	Variable x=NivelReadX()
	Variable y=NivelReadY()

	Print "----------------"
	Print "Please set the inclinometer readings to"
	Print "X: "+num2str(x-pitch)+" mrad"
	Print "Y: "+num2str(y-roll[0])+" mrad"

End

// ------------------------------------------------------
// NivelSetValueMan()
// ------------------------------------------------------
Function NivelSetValueMan()

	Wave roll = root:Multipole:MultipoleData:RollAngle
	Wave align = root:Multipole:quadAlignResults
	Wave/Z BenchAxis = root:Multipole:BenchAxis
	Wave/Z MagAxis = root:Multipole:MagAxis
	Variable dz = MagAxis[1] - BenchAxis[1]
	Variable pitch = dz / align[7] * 1000
	
	If( (!WaveExists(roll)) ||(!WaveExists(align)) )
		Return -1
	EndIf

	Variable x=0
	Variable y=0
	
	Prompt x, "X Reading"
	Prompt y, "Y Reading"
	DoPrompt "Inclinometer readings", x, y
	If (V_Flag)
		Return -1							
	EndIf

	Print "----------------"
	Print "Please set the inclinometer readings to"
	Print " "
	Print "X: "+num2str(x-pitch)+" mrad"
	Print "Y: "+num2str(y-roll[0])+" mrad"
	Print " "

End