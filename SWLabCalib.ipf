// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2017, ESRF - The European Synchroton, Gael Le Bec, Christophe Penel, Loic Lefebvre
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.6.4
// 13/09/2017

#pragma rtGlobals=3		// Use modern global access method and strict wave access.


Menu "SWLab Calibration"
	"Calibration", Calibration()
End 
//-------------------------------------------------------------------------------------
// Calibration()
// Calibration gives a curve of calibration of a magnet for a specific range of current / step
// LL, 03/07/2018
//-------------------------------------------------------------------------------------
Function Calibration()

	Variable Rmin, Rmax, Step
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z CalibParameters = root:Multipole:CalibrationFolder:CalibParameters
	
	
	if(!DataFolderExists("root:Multipole" ))
	NewDataFolder/o root:Multipole
	endif
	
	if(!DataFolderExists("root:Multipole:CalibrationFolder" ))
	NewDataFolder/o/s root:Multipole:CalibrationFolder
	endif

//1- Set parameters 
	String company = "ESRF"
	String operator = ""
	String polename = ""
	String npole="4-poles"
	String defaultDir = "C:\\Users\\SWUSER\\Documents\\Measurements"
	
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z CalibParameters = root:Multipole:CalibrationFolder:CalibParameters
	
	///////////////
	//First Dialog
	///////////////
	If ( !WaveExists(MeasStr) )
	Make/o/t/n=7 MeasStr
	EndIf
	company = MeasStr[0] 
	operator = MeasStr[1]
	polename = MeasStr[4] 
	npole = MeasStr[5]
	defaultDir = MeasStr[6] 
	Prompt company, "Company"
	Prompt operator, "Operator"
	Prompt npole,"N-poles"
	Prompt polename, "Multipole Serial Number"
	Prompt defaultDir, "Measurement Directory"
	DoPrompt "Initialization", operator, company, npole, polename, defaultDir
	If (V_Flag)
		killwaves/z MeasStr
		MultipoleClose()	
		Return -1					
	EndIf
	MeasStr[0] = company
	MeasStr[1] = operator
	MeasStr[4] = polename
	MeasStr[5] = npole
	MeasStr[6] = defaultDir
	
	If ( !WaveExists(CalibParameters) )
		Make/O/N=3 root:Multipole:CalibrationFolder:CalibParameters
		Wave/Z CalibParameters = root:Multipole:CalibrationFolder:CalibParameters
		CalibParameters[0] = 5  // Range min [A]
		CalibParameters[1] = 110 // Range Max [A]
		CalibParameters[2] = 5 	//  Step [A]
	EndIf
	Rmin = CalibParameters[0]
	Rmax = CalibParameters[1]
	Step = CalibParameters[2]

	// Prompt values

	Prompt Rmin, " Range min [A]"
	Prompt Rmax, "Range Max [A]"
	Prompt Step, "Step [A]" 
DoPrompt "Calibration measurement parameters",  Rmin, Rmax, Step
		If (V_Flag)
			Return -1							
		EndIf
		CalibParameters[0] = Rmin
		CalibParameters[1] = Rmax
		CalibParameters[2] = Step

	Rmin = CalibParameters[0]
	Rmax = CalibParameters[1]
	Step = CalibParameters[2]

//2 - set cycling current
PsReset()
sleep/S 5
PSOn()
sleep/S 10
setcyclingcurrent()
sleep/S 30
//3- set current and make the measurement - several times
variable iter=1
do
wave/z Current = root:Multipole:Current
current[0] = Rmax-(iter-1)*Step
PSSetcurrent(current[0])
sleep /s 600
Print time()+"...Measurement..."+num2str(current[0])+"A"
ci(0,1)
sleep/s 120
do
sleep/s 10
while(MultipoleIsMeasuring())
///////////////////////
If( !MultiIsMeasuring() )
	If (!cmpStr(MultiGetLastError(),"No Error\n"))
		MultiGetLastMeas()
		SetDataFolder root:Multipole
		// Convert field integral to [T mm]			
		//Wave FieldIntegral
		//FieldIntegral /= 1000
	else
		print "Error during circular measurement."
		MultipoleLastErr()
	endif
endif

Print time()+"     Analysis..."
SetDatafolder root:Multipole
// MultipoleAnalysis
MPMultipoleAnalysisDialog(0)
SetDataFolder root:Multipole:MultipoleData
MPQuadGradient(1)
Wave/z IntegratedGradient = root:Multipole:MultipoleData:IntegratedGradient
SetDataFolder root:Multipole:CalibrationFolder

make/o/N=(iter) CalibGrad
CalibGrad[iter-1]=IntegratedGradient[0]
make/o/N=(iter) CalibCurrent
CalibCurrent[iter-1]=current[0]

Execute "DuplicateDataFolder root:Multipole:MultipoleData, root:Multipole:CalibrationFolder:MultipoleData"+num2str(current[0])+"A"
killDataFolder root:Multipole:MultipoleData
iter=iter+1							// execute the loop body
while (Rmin-1<Rmax-(iter-1)*Step)

PSoff()

string Title 
Title = "Calibration curve for "+MeasStr[4]
Display/n=GCalib root:Multipole:CalibrationFolder:CalibGrad vs root:Multipole:CalibrationFolder:CalibCurrent as "Graph"+Title
Label left "CalibGrad_[T]"
Label bottom "CalibCurrent_[A]"
Edit/n=TCalib root:Multipole:CalibrationFolder:CalibGrad,root:Multipole:CalibrationFolder:CalibCurrent as "Table"+Title

Execute "LayoutCalib(MeasStr[4])"
String printerNameStr ="PDFCreator" 
PrintSettings setPrinter=printerNameStr 
PrintLayout LayoutCalib

NewPath/O/C savePath,"P:\\MACH\\ID\\bench\\1-ESRF\\Measurements\\4-Magnet cycling"
string TitleExp=Title+".pxp"
SaveExperiment/P=savePath as TitleExp
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Window LayoutCalib(title) : Layout
	String title 
	String BigTitle="\\Z18 "+ title
	PauseUpdate; Silent 1		// building window...
	Layout/C=1/W=(535.5,75.5,1191.75,476.75) GCalib(71.25,119.25,522.75,363)/O=1,TCalib(165.75,380.25,396.75,683.25)/O=2 as title
	TextBox/C/N=text0/F=0/A=LB/X=23.59/Y=94.20 BigTitle
EndMacro

