// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2016, ESRF - The European Synchroton, Gael Le Bec, Christophe Penel, Loic Lefebvre
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.5.7
// 23/02/2017

#pragma rtGlobals=1		// Use modern global access method.
Menu "SWLab Motion"
	//"Abort", Axis2dAbort(root:xps2d:path)
	"Abort", MultipoleAbort()
	//"---------------"
	//SubMenu "Initialization"
		//"Open", Axis2dopen("XPS2d")
		//"Close",Axis2dClose(root:xps2d:path)
		"Reference",Axis2dRef(root:xps2d:path)
		//"Display XPS Panel",Axis2dopen("XPS2dpan");XPSPanel()
	//End
	"-------------"
	SubMenu "Move to Centre"
		"No Sag", SWMoveToCentre(0,0)
		"Correct Sag", SWMoveToCentre(1,1)
	End
	"-------------"
	SubMenu "Position"
		"Set Absolute Position", P_MoveAbs("root:xps2d:path", "w", ,)
		"Set Relative Position", P_MoveRel("root:xps2d:path", "w", ,)
		"Read Position", P_readPos("root:xps2d:path")
	End
	SubMenu "Pitch and Yaw"
		"Set Pitch and Yaw", P_SetTaper("root:xps2d:path", ,)
		"Read Pitch and Yaw", P_readTaper("root:xps2d:path")
	End
	SubMenu "Velocity"
		"Set Velocity", P_SetVelocity("root:xps2d:path", ,)
		"Read Velocity", P_readVelocity("root:xps2d:path")
	End
	SubMenu "Acceleration"
		"Set Acceleration", P_SetAccel("root:xps2d:path", ,)
		"Read Acceleration", P_readAccel("root:xps2d:path")
	End
	SubMenu "Trajectories"
		SubMenu "Line"
			"Line",P_LineTo("root:xps2d:path", , , ,)
			"Line And Trig",P_LineAndTrig("root:xps2d:path",,,,,,,,,)
		End
		SubMenu "Circle"
			"Circle",P_Circle("root:xps2d:path", , , ,)
			"Circle And Trig",P_CircleAndTrig("root:xps2d:path", , , , , , , , ,  )
			"Circle And Trig - Master Stages",P_MasterCircleAndTrig("root:xps2d:path", , , , , , , , ,  )
		End
		SubMenu "Trajectories"
			"Send Trajectory",P_SendTraj("root:xps2d:path",,)
			"Run Trajectory",P_ExecTraj("root:xps2d:path", , , ,)
		End
	End
	SubMenu "Tools"
		"Motion in Progress?", P_IsMoving("root:xps2d:path")
		"Get Gathering Data", Axis2dGetGatheringData(root:xps2d:path )
	End

	
End

//MOVING Function and proc

Function IsMoving(path)
	variable  path
	return Axis2dIsMoving(path)
End

proc P_IsMoving(path)
string  path

	if (Axis2dIsMoving($path))
		print "Moving"
	else
		print "Stopped"
	endif
end

// POSITION FUNCTION AND PROC 

Function /C ReadPos(path)
	variable  path
	return Axis2dReadpos(path)
End

proc  P_ReadPos(path)
string path
variable /C XPS2dpos
	XPS2dpos= Axis2dReadPos($path)
		print "X= ", real(XPS2dpos)," mm    Z= ",imag(XPS2dpos) ,"  mm" 
End

Function  MoveAbs(path,who,X,Z)
	variable  path
	string who
	variable X
	variable Z
	return Axis2dMoveAbs(path,char2num(who),cmplx(X,Z))
End

proc  P_MoveAbs(path,who,X,Z)
string path
string who
variable X=Nan
variable Z=Nan

	if (!Axis2dMoveAbs($path,char2num(who),cmplx(X,Z)))
		print  "NOT_OK"
	endif	
End


Function  MoveRel(path,who,X,Z)
	variable  path
	string who
	variable X
	variable Z
	return Axis2dMoveRel(path,char2num(who),cmplx(X,Z))
End

proc  P_MoveRel(path,who,X,Z)
string path
string who
variable X=Nan
variable Z=Nan

	if (!Axis2dMoveRel($path,char2num(who),cmplx(X,Z)))
		print  "NOT_OK"
	endif	
End
//VELOCITY FUNCTION AND PROCS

Function  SetVelocity(path,X,Z)
	variable  path
	variable X
	variable Z
	return Axis2dSetvelocity(path,cmplx(X,Z))
End

proc  P_SetVelocity(path,VX,VZ)
string path
variable VX=Nan
variable VZ=Nan

	if (!Axis2dSetVelocity($path,cmplx(VX,VZ)))
		print  "NOT_OK"
	endif	
End

Function /C ReadVelocity(path)
	variable  path
	return Axis2dReadVelocity(path)
End

proc  P_ReadVelocity(path)
string path
variable /C XPS2dpos
	XPS2dpos= Axis2dReadVelocity($path)
		print "V_X= ", real(XPS2dpos)," mm/s    V_Z= ",imag(XPS2dpos) ,"  mm/s" 
End

// ACCELERATION FUNCTION AND PROCs
Function  SetAccel(path,X,Z)
	variable  path
	variable X
	variable Z
	return Axis2dSetAccel(path,cmplx(X,Z))
End

proc  P_SetAccel(path,AX,AZ)
string path
variable AX=Nan
variable AZ=Nan

	if (!Axis2dSetAccel($path,cmplx(AX,AZ)))
		print  "NOT_OK"
	endif	
End

Function /C ReadAccel(path)
	variable  path
	return Axis2dReadAccel(path)
End

proc  P_ReadAccel(path)
string path
variable /C XPS2dpos
	XPS2dpos= Axis2dReadAccel($path)
		print "V_X= ", real(XPS2dpos)," mm/s    V_Z= ",imag(XPS2dpos) ,"  mm/s" 
End

//

Proc P_SendTraj(path,ftraj,traj)
string path
string ftraj ="traj.trj"
string  traj="root:xps2d:traj"

	if( !Axis2DCreateTraj($path,ftraj,$traj))
		print "error during transfert"
	endif

end proc

Proc P_ExecTraj(path,ftraj,Velocity,accel,N,background)
string path
string ftraj ="traj.trj"
string  traj="root:xps2d:traj"
variable Velocity
Variable Accel,
Variable N
variable background

	if( Axis2DExecTraj($path,ftraj,Velocity,accel,N,background))
	
		if(background!=0)
			print "trajectorie started"
		else
			print "trajectorie ended"
		endif
	else
		print "error"
	endif

end proc

// Taper FUNCTION AND PROCs
Function  SetTaper(path,X,Z)
	variable  path
	variable X
	variable Z
	return Axis2dSetTaper(path,cmplx(X,Z))
End

proc  P_SetTaper(path,AX,AZ)
string path
variable AX=Nan
variable AZ=Nan

	if (!Axis2dSetTaper($path,cmplx(AX,AZ)))
		print  "NOT_OK"
	endif	
End

Function /C ReadTaper(path)
	variable  path
	return Axis2dReadTaper(path)
End

proc  P_ReadTaper(path)
string path
variable /C XPS2dpos
	XPS2dpos= Axis2dReadTaper($path)
		print "Taper_X= ", real(XPS2dpos)," mm    Taper_Z= ",imag(XPS2dpos) ,"  mm" 
End

#pragma rtGlobals=1		// Use modern global access method.

Function CheckProc(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
	if (checked )
		XPSstartrefresh()
	else
		XPSstopRefresh()
	endif
End

Function XPSstartrefresh()

end


Function XPSstopRefresh()
	KillBackground
end

function XPSRefresh()
NVAR /C Position=root:XPS2dpan:Position
NVAR /C path=root:XPS2dpan:path
NVAR /C Taper=root:XPS2dpan:Taper
NVAR /C Acceleration=root:XPS2dpan:Acceleration
NVAR /C Velocity=root:XPS2dpan:Velocity

	Position=axis2dreadpos(path)
	Taper=axis2dreadTaper(path)
	Velocity=axis2dreadVelocity(path)
	Acceleration=axis2dreadaccel(path)
	
	return 0
	
end

Function ButtonProc_2(ctrlName) : ButtonControl
	String ctrlName
	
	print ctrlname
	KillBackground
	killWindow XPS
	NVAR  mypath=root:XPS2dpan:path
	axis2dclose(mypath)
End

Proc P_LineTo(path,XP,ZP,Vel,Accel,background)
	String path
	Variable XP
	Variable ZP
	Variable Vel
	Variable Accel
	Variable background
	
	Axis2dlineto($path,cmplx(XP,ZP),Vel,Accel,background)
	
End

Proc P_LineAndTrig(path,XPStart,ZPStart,Firstlen,Step,nb,XPend,ZPend,Vel,Accel)
	String path
	Variable XPStart
	Variable ZPStart
	Variable Firstlen
	Variable step
	variable nb
	Variable XPEnd
	VAriable ZPEnd
	Variable Vel
	Variable Accel
	
	Axis2dlineandtrig($path,cmplx(XPStart,ZPStart),Firstlen,step,nb,cmplx(XPEnd,ZPEnd),Vel,Accel)
	
End

Proc P_circle(path,XC,ZC,Teta,Vel,Accel,background)
	String path
	Variable XC
	Variable ZC
	Variable Teta
	Variable Vel
	Variable Accel
	Variable background
	
	Axis2dcircle($path,cmplx(XC,ZC),Teta,Vel,Accel,background)
	
End
Proc P_circleandtrig(path,XC,ZC,R,TetaBeg,TetaFirstTrig,TetaTrigLen,TrigNumber,TetaEnd,Vel)
	String path
	Variable XC
	Variable ZC
	Variable R
	Variable TetaBeg
	Variable TetaFirstTrig
	Variable TetaTrigLen
	Variable TrigNumber
	Variable 	TetaEnd
	Variable Vel
	
	Variable accel=Min(Vel*10,400)
	
	Axis2dcircleAndTrig($Path,cmplx(XC,ZC),R,TetaBeg,TetaFirstTrig,TetaTrigLen,TrigNumber,TetaEnd,Vel,accel)
	
End
Proc P_Mastercircleandtrig(path,XC,ZC,R,TetaBeg,TetaFirstTrig,TetaTrigLen,TrigNumber,TetaEnd,Vel)
	String path
	Variable XC
	Variable ZC
	Variable R
	Variable TetaBeg
	Variable TetaFirstTrig
	Variable TetaTrigLen
	Variable TrigNumber
	Variable 	TetaEnd
	Variable Vel
	
	Variable accel=Min(Vel*10,400)
	
	Axis2dMastercircleAndTrig($Path,cmplx(XC,ZC),R,TetaBeg,TetaFirstTrig,TetaTrigLen,TrigNumber,TetaEnd,Vel,accel)
	
End

Window XPSPanel() : Panel
	PauseUpdate; Silent 1		// building window...
	if (strlen(winlist("XPS","",""))==0)
	NewPanel /K=2 /W=(827,98,1110,244) /N=XPS	
	ValDisplay XPOS,pos={19,5},size={108,15},title="XPOS"
	ValDisplay XPOS,limits={0,0,0},barmisc={0,1000}
	ValDisplay XPOS,value= #"real(root:XPS2dpan:Position)"
	ValDisplay ZPOS,pos={175,4},size={104,15},title="Z POS"
	ValDisplay ZPOS,limits={0,0,0},barmisc={0,1000}
	ValDisplay ZPOS,value= #"imag(root:XPS2dpan:Position)"
	ValDisplay ZTAP,pos={179,34},size={100,15},title="ZTAP"
	ValDisplay ZTAP,limits={0,0,0},barmisc={0,1000}
	ValDisplay ZTAP,value= #"imag(root:XPS2dpan:Taper)"
	ValDisplay XTAP,pos={19,35},size={108,15},title="XTAP"
	ValDisplay XTAP,limits={0,0,0},barmisc={0,1000}
	ValDisplay XTAP,value= #"real(root:XPS2dpan:Taper)"
	CheckBox Refresh,pos={60,129},size={55,14},proc=CheckProc,title="Refresh"
	CheckBox Refresh,value= 1
	Button Close,pos={177,123},size={50,20},proc=ButtonProc_2,title="Close"
	ValDisplay XVel,pos={23,63},size={104,15},title="XVel"
	ValDisplay XVel,limits={0,0,0},barmisc={0,1000}
	ValDisplay XVel,value= #"real(root:XPS2dpan:Velocity)"
	ValDisplay ZVel,pos={183,61},size={96,15},title="ZVel"
	ValDisplay ZVel,limits={0,0,0},barmisc={0,1000}
	ValDisplay ZVel,value= #"imag(root:XPS2dpan:Velocity)"
	ValDisplay Xaccel,pos={12,96},size={117,15},title="Xaccel"
	ValDisplay Xaccel,limits={0,0,0},barmisc={0,1000}
	ValDisplay Xaccel,value= #"real(root:XPS2dpan:Acceleration)"
	ValDisplay Zaccel,pos={172,93},size={108,15},title="Zaccel"
	ValDisplay Zaccel,limits={0,0,0},barmisc={0,1000}
	ValDisplay Zaccel,value= #"imag(root:XPS2dpan:Acceleration)"
	SetBackground XPSRefresh()
	CtrlBackground period=15,start
	else
		
	endif
	
EndMacro

//------------------------------------------------------------------------------------
//  SWMoveToCentre(correctSag)
//------------------------------------------------------------------------------------
Function SWMoveToCentre(correctSag, promptSag)
	Variable correctSag, promptSag
	
	// Initialize
	Variable xa = 0
	Variable za = 0
	Wave/z MagAxisLastMeas = root:Multipole:MagAxisLastMeas
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z MeasParameters = root:Multipole:MeasParameters
	Variable f
	
	If (WaveExists(MeasParameters))
		f=MeasParameters[8] // [Hz]
	Else	
		f = 1e4
	EndIf
	Variable g = 9.81 // [m/s^2]
	Variable xc = 0
	Variable zc = 0
	
	Wave/Z MagCentreLastMeas = root:Multipole:MagCentreLastMeas
	Wave/Z MagCentre = root:Multipole:MagCentre	
	NVAR/z xpsPath = root:xps2d:path
	
	Variable sag
	
	//------------------------------------------------------------------------------------
	//  SWMoveToAxis

	If( !WaveExists(MagAxisLastMeas) )
		//Print "The MagAxisLastMeas wave is missing"
		If (!WaveExists(MagAxis))
		//	Print "The MagAxis wave is missing"
			xa=0
			za=0
		//	Print "Wire moved to (0, 0)"
		Else
	//		Print "Wire moved to default values (MagAxis[0], MagAxis[1])"		
			xa = MagAxis[0]
			za = MagAxis[1]
		EndIf
	ElseIf ( abs(MagAxisLastMeas[0]) < 45 && abs(MagAxisLastMeas[1]) < 45 )
		xa = MagAxisLastMeas[0]
		za = MagAxisLastMeas[1]
	Else 
		xa = MagAxis[0]
		za = MagAxis[1]	
	EndIf

	// Move to axis
	Axis2dSetTaper(xpsPath,cmplx(xa,za))
	Do
		Sleep/S 0.5 
	While ( Axis2dIsMoving(xpsPath) )
	//------------------------------------------------------------------------------------
	
	If( !WaveExists(MagCentreLastMeas) )
		//Print "The MagCentreLastMeas wave is missing"
		If (!WaveExists(MagCentre))
		//	Print "The MagCentre wave is missing"
		//	Print "Wire moved to (0, 0)"
		Else
		//	Print "Wire moved to default values (MagCentre[0], MagCentre[1])"		
			xc = MagCentre[0]
			zc = MagCentre[1]
		EndIf
	ElseIf (  ( abs(MagCentreLastMeas[0]) < 45 )  &&  ( abs(  MagCentreLastMeas[1] < 45 )  ) )   
		xc = MagCentreLastMeas[0]
		zc = MagCentreLastMeas[1]
	Else 
		xc = MagCentre[0]
		zc = MagCentre[1]
	EndIf

	If (!correctSag)
		//--- Sag not corrected
		Axis2dMoveAbs(xpsPath,char2num("w") ,cmplx(xc, zc) )
	Else
		//--- Sag corrected
		If (promptSag)
			Prompt f, "f0 [Hz]: "
			DoPrompt "Wire 1st mode",  f
			If (V_Flag)
				Return -1							
			EndIf
			MeasParameters[8] = f
		EndIf
		// Compute Sag [mm]
		sag = g/(32*f^2)*1000
		//Print "Corrected sag: "+num2str(sag)+" mm"
		// Move
		zc -= sag
		Axis2dMoveAbs(xpsPath,char2num("w") ,cmplx(xc, zc) )
	EndIf
	
End

