// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2016, ESRF - The European Synchroton, Christophe Penel, Loic Lefebvre, Gael Le Bec
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.6.4
// 11/01/2018

#pragma rtGlobals=3		// Use modern global access method and strict wave access.
Menu "SWLab Auto"
	"Octupole measurement" ,OctuTaskPanel()
end

//---------------------------------------------------------
// Build the TaskPanel
//---------------------------------------------------------
Window OctuTaskPanel() : Panel
	// Check initialization	
	// MultipoleCheckOpen()
	// sleep/s 5
	If (!DataFolderExists("root:Multipole")||(numType(root:K2182:path)==2)||(numType(root:XPS2d:path)==2))
		Print "Please initialize the bench with the following command:"
		Print "SWLab Measurement > Initialization > Open"
		Return -1
	EndIf
	// Load Measurement Parameters
	MultipoleLoadParam()
	// Switch to quiet mode
	MultiQuiet(1)
	// Date and time
	Print  Date()+", "+Time()
	
	if (strlen(WinList("OctuTaskPanel",";","WIN: 64" ))>0)
		DoWindow /F OctuTaskPanel
		return
	endif
	PauseUpdate; Silent 1		
	// building window...
	if (!datafolderExists("root:Multipole:OctuTaskPanel"))
		Newdatafolder root:Multipole:OctuTaskPanel
		make/T /N=4 root:Multipole:OctuTaskPanel:Tasklabel
		make /N=4 root:Multipole:OctuTaskPanel:TaskFollow
		root:Multipole:OctuTaskPanel:TaskFollow=0
		variable /G root:Multipole:OctuTaskPanel:Mytoggle
		variable /G root:Multipole:OctuTaskPanel:CurrentIter
		variable /G root:Multipole:OctuTaskPanel:CurrentTask
		 root:Multipole:OctuTaskPanel:Tasklabel[0]="Set Power Supply"
		 root:Multipole:OctuTaskPanel:Tasklabel[1]="Measurement"
		 root:Multipole:OctuTaskPanel:Tasklabel[2]="Check results"
		 root:Multipole:OctuTaskPanel:Tasklabel[3]="Save"
		 variable /G  root:Multipole:OctuTaskPanel:Toggle0
		 variable /G  root:Multipole:OctuTaskPanel:Toggle1
		 variable /G  root:Multipole:OctuTaskPanel:Toggle2
		 variable /G  root:Multipole:OctuTaskPanel:Toggle3
		 
	endif	
	variable Ntask=numpnts(root:Multipole:OctuTaskPanel:TaskLabel)
	root:Multipole:OctuTaskPanel:Mytoggle=0
	
	NewPanel /K=1 /W=(400,148,750,168+25*NTask)
	//	ShowTools/A	
	string cmd
	variable ind=0
	//--- Build check boxes
	do
		cmd= "CheckBox  taskcheck"+num2str(ind)+",pos={30,15+"+num2str(25*ind)+"},size={ 40,14},disable=2,title=\""+root:Multipole:OctuTaskPanel:TaskLabel[ind]+"\",variable=root:Multipole:OctuTaskPanel:toggle"+num2str(ind)+",mode=1, fStyle=1"
		Execute/Q cmd
		ind+=1
	while( ind<ntask)
	Variable /G  root:Multipole:OctuTaskPanel:SaveOctuChoice=0
	//--- Start and Stop buttons 
	Button StartTasks,pos={257,11},size={65,31},proc=StartOctuTaskButton,title="Start"
	Button StopTasks,pos={257,50},size={65,31},proc=StopOctuTaskButton,title="Stop"

	OctuTaskTo( root:Multipole:OctuTaskPanel:CurrentTask)

	
EndMacro

//------------------------------------------------------------------------------------------
// OctuCenterAxisTask(s)
//------------------------------------------------------------------------------------------
Function OctuCenterAxisTask(s)
STRUCT WMBackgroundStruct &s

//--- Initialization 
NVAR StepTask=root:Multipole:OctuTaskPanel:CurrentTask
Wave/Z Param=root:Multipole:CenterMeasParameters
Wave/Z QC=root:Multipole:MagCentre
Wave/Z QA=root:Multipole:MagAxis
NVAR CurrentIter=root:Multipole:OctuTaskPanel:CurrentIter
NVAR SaveOctuChoice =root:Multipole:OctuTaskPanel:SaveOctuChoice
NVAR mytoggle=root:Multipole:OctuTaskPanel:Mytoggle
Variable NBITER=param[7]
wave/Z TXC= root:Multipole:IterativeAlignment:MagCentreX
wave/Z TZC= root:Multipole:IterativeAlignment:MagCentreZ
wave/Z TXA= root:Multipole:IterativeAlignment:MagAxisX
wave/Z TZA= root:Multipole:IterativeAlignment:MagAxisZ
Wave/Z current = root:Multipole:Current
Wave/Z/t MeasStr=root:Multipole:MeasStr
Variable curr=current[0]
Variable cycle=0
Wave/Z ExportParameters=root:Multipole:ExportParameters
Variable rSpec = ExportParameters[4]
NVAR/Z FieldIntDisplay = root:Multipole:OctuTaskPanel:FieldIntDisplay
If ( !NVAR_Exists(FieldIntDisplay) )
	Variable/G root:Multipole:OctuTaskPanel:FieldIntDisplay = 0
EndIf
NVAR/Z TimerVar=root:Multipole:OctuTaskPanel:TimerVar
If ( !NVAR_Exists(TimerVar) )
	Variable/G root:Multipole:OctuTaskPanel:TimerVar
EndIf
//Make/o/n=2 MagCentreTmp
curr = current[0]
NVAR/Z curr0 =  root:Multipole:QuadTaskPanel:curr0
If  ( !NVAR_Exists(curr0) )
	Variable/G root:Multipole:QuadTaskPanel:curr0 = curr
EndIf
Variable CyclingWaitTime
//--- Change the color of the Start button at each call
if (mytoggle==0)
	mytoggle=1
	ModifyControl/Z StartTasks,fColor=(0,5000,0)	
else
	mytoggle=0
	ModifyControl/Z StartTasks,fColor=(52224,52224,52224)
endif

// --- Finite State Machine
//
// 0		INITIALIZATION
// 0 		Initialization
// 0.1	PS ready
//
// 1		CIRCULAR MEASUREMENTS
// 1 		Launch circular measurements
// 1.1	Wait  for circular measurement results
// 1.2	Multipole analysis and tests vs specifications
//
// 2		VALIDATION
// 2		Open measurement validation dialog
// 2.1 	User validation
//
// 3		SAVE
// 3		Export Measurement results
strswitch(num2str(steptask))			// numeric switch
		// -------------------------------
		// State 0
		// Initialization
		// -------------------------------
		// 0 - Init and set power suppy
		case "0":
			Print time()+"     Initialize..."
			ModifyControl StartTasks,fColor=(0,5000,0)				
			// Ask metadata
			if (MultipoleMetaData()==-1)
				ModifyControl StartTasks,fColor=(52224,52224,52224)
				Return -1							
			EndIf
			Duplicate/o root:Multipole:BenchCentre root:Multipole:MagCentre
			Duplicate/o root:Multipole:BenchAxis root:Multipole:MagAxis
			current[0] = curr
			// Set power supply
			// 
			//------------------------------------------------------------------
			// CALL POWER SUPPLY FUNCTIONS HERE
			//-----------------------------------------------------------------
			//
			OctuTaskTo(0.1)
			return 0
		break 
		// 0.1 - Set Current
		case "0.1":
			// Prepare Power Supply
			Prompt cycle, "Set current (0: no, 1: yes)"
			DoPrompt "Power Supply", cycle
			If (V_Flag)
				OctuTaskTo(0)
				Return -1							
			EndIf
			curr0 = curr
			If (cycle == 1)
				OctuTaskTo(0.2)
			ElseIf (cycle == 0)
				OctuTaskTo(1)
			Else
				print "Answer must be '0' or '1' "
				SextTaskTo(0.1)			
			EndIf
			return 0
		break
		// 0.2 - PS set current
		case "0.2":
			Print time()+"     Set current..." 
			SetCurrentOF()
			//--------------
			TimerVar = DateTime
			OctuTaskTo(0.3)
			return 0
		break
		// 0.3 - Wait for current stability
		case "0.3":
			CyclingWaitTime = 60 // s
			If( DateTime > TimerVar + CyclingWaitTime)
				OctuTaskTo(1)
			EndIf
			return 0
			
		// -------------------------------
		// State 1
		// Circular Measurements
		// -------------------------------
		// 1 - Launch circular measurements
		case "1":  
				Print time()+"     Circular measurements..." 
				MultipoleSetCentreAxis()	
				//Print  Date()+", "+Time()
				Wave/Z/t MeasStr=root:Multipole:MeasStr
				MeasStr[2] = Date()
				MeasStr[3] = Time()
			CI(0,0)
			OctuTaskto(1.1)
			return 0
		// 1.1 - Wait  for circular measurement results
		case "1.1":
			If( !MultiIsMeasuring() )
				If (!cmpStr(MultiGetLastError(),"No Error\n"))
					MultiGetLastMeas()
					SetDataFolder root:Multipole
					// Convert field integral to [T mm]
					//Wave FieldIntegral
					//FieldIntegral /= 1000
					OctuTaskto(1.2)
					return 0
				else
					print "  MultiRepeatCircularScan " ,param[0],param[1],"Radius",param[2],param[3],param[4],param[5],param[6],param[7],")"
					MultipoleLastErr()
					ModifyControl StartTasks,fColor=(52224,52224,52224)
					return 2				
				endif
			endif
			return 0

		break;
		// 1.2 - Multipole analysis and tests vs specifications
		case"1.2":
				Print time()+"     Analysis..." 
				SetDatafolder root:Multipole
				// MultipoleAnalysis
				MPMultipoleAnalysisDialog(0)
				MPMultiFindCentre(0)
				MultipoleSetCentreAxis()	
				// Compute multipoles at specified radius
				SetDataFolder root:Multipole:MultipoleData
				MPChangeRadius(rSpec,0)
				// Strength and roll angle
				MPOctuStrength(0)
				MPOctuRoll(0)
				// Check results and compute shims
				MultipoleCheckOctuResult()
				// Move the wire to the magnet centre 
				Print "WIRE MOVED TO THE MAGNET CENTRE"
				SWMoveToCentre(1,0)
				OctuTaskTo(2)
				return 0
		break;
		// -------------------------------
		// State 2
		// Validation by the user
		// -------------------------------
		// 2 - Open measurement validation dialog
		case "2":
			SaveOctuChoice=0
			Execute/Q "VALIDATEOCTUMEAS()"
			Octutaskto(2.1)
			return 0		
		break
		// 2.1 - User validation
		case "2.1":
			switch (SaveOctuChoice)
			case 0 :
				return 0
				break
			case 1 :
				MultipoleSetCentreAxis()	
				OctuTaskto(3)
				return 0
				break
			case 2 :
				MultipoleSetCentreAxis()	
				OctuTaskto(1)
				return 0
				break
			case 3:
				print "Measurement cancelled"
				ModifyControl StartTasks,fColor=(52224,52224,52224)
				OctuTaskto(3)
				return 2
				break
			endswitch			
		// -------------------------------
		// State 3
		// Export measurement results
		// -------------------------------		
		case "3":
				SetDataFolder root:Multipole:MultipoleData
				MPExportData()	
				Print time()+"     Export..." 
				Variable turnOff = 1
				Prompt turnOff, "Turn off the power supply? (0: no, 1: yes)"
				DoPrompt "Power Supply", turnOff
				If (V_Flag)
					turnOff = 0	
				EndIf
				If ( turnOff )
					PSOff()
					cycle = 0
				EndIf
				ModifyControl StartTasks,fColor=(52224,52224,52224)
				Print "-----------------------------"
				Print " "
				Print "MEASUREMENT COMPLETED"
				If ( turnOff )
				Print "THE POWER SUPPLY AS BEEN TURNED OFF"
				EndIf
				Print " "
				Print "-----------------------------" 
				//DoAlert  0, "Measurement done"
				Print "-----------------------------"
				Print "MEASUREMENT DONE"
				OctuTaskto(0)
				return 1
		break;	

	endswitch

return 0

end

//

//-----------------------------------------------------------------------------------------------------------------------------------
function OctuTaskTo(num)
variable num
variable val

variable i
DoWindow /F OctuTaskPanel
variable nbt=numpnts(root:Multipole:OctuTaskPanel:TaskLabel)

if (num >nbt-1)
	DoAlert 0 , "BAD TASK NUM "
	return 0
endif
NVAR CurrentTask=root:Multipole:OctuTaskPanel:CurrentTask
string cmd
CurrentTask=Num
for (i=0;i<nbt;i+=1)
	cmd="root:Multipole:OctuTaskPanel:toggle"+num2str(i)+"=0"
	execute cmd	
endfor
if (num>=0 )
	cmd="root:Multipole:OctuTaskPanel:toggle"+num2str(floor(num))+"=1"
	execute cmd
endif
end
//-----------------------------------------------------------------------------------------------------------------------------------
Function StartOctuTaskButton(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	Variable numTicks = 60 
	Wave/Z Param=root:Multipole:CenterMeasParameters
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			//NewDataFolder/O root:Multipole:IterativeAlignment
			//Make/O/N=(param[7]) root:Multipole:IterativeAlignment:MagCentreX
			//Make/O/N=(param[7]) root:Multipole:IterativeAlignment:MagCentreZ
			//Make/O/N=(param[7]) root:Multipole:IterativeAlignment:MagAxisX
			//Make/O/N=(param[7])root:Multipole:IterativeAlignment:MagAxisZ
			CtrlNamedBackground OctuFindCentreAxis, period=numTicks, proc=OctuCenterAxisTask
			CtrlNamedBackground OctuFindCentreAxis, start
			OctuTaskTo(0)
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End
//-----------------------------------------------------------------------------------------------------------------------------------
Function StopOctuTaskButton(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			 ModifyControl StartTasks,fColor=(52224,52224,52224)
			 CtrlNamedBackground OctuCenterAxisTask, kill
			 MultiAbort()
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End

Window VALIDATEOCTUMEAS() : Panel
	PauseUpdate; Silent 1		// building window...
	if (strlen(WinList("VALIDATEOCTUMEAS",";","WIN: 64" ))>0)
		DoWindow /F VALIDATEOCTUMEAS
		return
	endif

	NewPanel /K=1/W=(600,200,950,360) As "Check results"
	DrawText 32, 20, "Please check the results printed in the Igor history"
	Button SAVEMEAS,pos={50,40},size={150,25},proc=OctuCHOICE,title="Continue"
	Button REDOMEAS,pos={50,80},size={150,25},proc=OctuCHOICE,title="Redo"
	Button STOPMEAS,pos={50,120},size={150,25},proc=OctuCHOICE,title="Cancel"
EndMacro

Function OctuCHOICE(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	NVAR SAVEOctuCHOICE=  root:Multipole:OctuTaskPanel:SAVEOctuCHOICE
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			strswitch(ba.CtrlName)
				case "SAVEMEAS":
					SAVEOctuCHOICE=1
					DoWindow/K VALIDATEOCTUMEAS
					break
			
				case "REDOMEAS":
					SAVEOctuCHOICE=2
					DoWindow/K VALIDATEOCTUMEAS
					break
					
				case  "STOPMEAS":
					SAVEOCTUCHOICE=3
					DoWindow/K VALIDATEOctuMEAS
					break
			endswitch	
						
		case -1: // control being killed
			break
	endswitch

	return 0
End





// ------------------------------------------------------------------------------
// MultipoleCheckOctuResult()
// ------------------------------------------------------------------------------
Function MultipoleCheckOctuResult()
	
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z current = root:Multipole:Current
	Wave/Z MagCentre = root:Multipole:MagCentre
	//Wave MagAxis = root:Multipole:MagAxis
	Wave/Z roll = root:Multipole:MultipoleData:RollAngle
	Wave/Z strength = root:Multipole:MultipoleData:IntegratedStrength
	Wave/Z an = root:Multipole:MultipoleData:anNorm_13mm
	Wave/Z bn = root:Multipole:MultipoleData:bnNorm_13mm	
	Wave/Z BenchCentre = root:Multipole:BenchCentre
	Wave/Z BenchAxis = root:Multipole:BenchAxis
	Wave/Z ExportParameters = root:Multipole:ExportParameters
	Wave/Z MeasParameters = root:Multipole:MeasParameters
	Variable f=MeasParameters[8] // [Hz]
	Variable g = 9.81 // [m/s^2]
	Variable sag=g/(32*f^2)*1000 // [mm]
	
	
	Make/o/n=3 root:Multipole:TestResult
	Wave/Z TestResult = root:Multipole:TestResult
	TestResult = 1
	
	// Magnet width [mm]
	Variable MagWidth = ExportParameters[6]
	
	// Multipole test
	String MultipoleResult = "YES"
//	If (abs(an[3])>22||abs(bn[3])>22||abs(an[4])>12||abs(bn[4])>12||abs(an[5])>5||abs(bn[5])>5)
//		MultipoleResult = "NO"
//		TestResult[2] = 0
//	EndIf
	// Centering test
	String CentreResult = "YES"
	If (abs(MagCentre[1]-sag-BenchCentre[1]) > 0.1)
		CentreResult = "NO"
		TestResult[0] = 0
	EndIf
	// Roll test
	String RollResult = "YES"
	If (abs(roll[0]) > 130e-3)
		RollResult = "NO"	
		TestResult[1] = 0
	EndIf

	Print "-----------------------------"
	Print "OCTUPOLE MEASUREMENTS"
	Print Secs2Date(DateTime,1)
	Print Secs2Time(DateTime,1)
	Print "SERIAL NUMBER: "+MeasStr[4]
	Print "CURRENT:            "+num2Str(Current[0])+" A"
	Print "CENTRE:              "+ "x0: "+ num2Str(MagCentre[0]-BenchCentre[0])+" mm;    "+"z0: "+ num2Str(MagCentre[1]-sag-BenchCentre[1])+" mm"
	//Print "AXIS:                    "+"dx0: "+num2Str(MagAxis[0]-BenchAxis[0])+" mm;   "+"dz0: "+num2Str(MagAxis[1]-BenchAxis[1])+" mm"
	Print "ROLL ANGLE:       "+num2Str(roll[0])+" mrad"
	Print "STRENGTH:          "+num2Str(strength[0])+" T/m^2" 
	Print "MULTIPOLES AT 13 mm"
	Print "b1: "+num2Str(bn[0])+" units;   a1: "+num2Str(an[0])+" units"
	Print "b2: "+num2Str(bn[1])+" units;   a2: "+num2Str(an[1])+" units"
	Print "b3:  "+num2Str(bn[2])+" units;   a3: "+num2Str(an[2])+" units"
	Print "b4: "+num2Str(bn[3])+" units;   a4: "+num2Str(an[3])+" units"
	Print "b5: "+num2Str(bn[4])+" units;   a5: "+num2Str(an[4])+" units"	
	Print "b6: "+num2Str(bn[5])+" units;   a6: "+num2Str(an[5])+" units"	
//	Print "MULTIPOLES WITHIN SPECIFICATIONS: "+MultipoleResult+"."
	Print "MAGNETIC CENTRE WITHIN SPECIFICATIONS: "+CentreResult+"."
	Print "ROLL ANGLE WITHIN SPECIFICATIONS: "+RollResult+"."
	Print "-----------------------------"
	If(StringMatch(CentreResult, "NO")||StringMatch(RollResult, "NO"))
		Print "PLEASE INSERT SHIMS: dZ(-x0) = "+num2str( BenchCentre[1]-MagCentre[1] + sag - roll[0]/1000 * MagWidth/2)+" mm and dZ(x0) = "+num2str( BenchCentre[1]-MagCentre[1] + sag + roll[0]/1000 * MagWidth/2)+"mm"
	EndIf
	Print "-----------------------------"
	// Clear
	KillStrings/z MultipoleResult, CentreResult, RollResult
End

